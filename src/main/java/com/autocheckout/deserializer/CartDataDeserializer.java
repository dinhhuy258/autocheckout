package com.autocheckout.deserializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.autocheckout.data.lazada.cart.CartItemData;
import com.autocheckout.data.lazada.cart.CartItemDataFields;
import com.autocheckout.data.lazada.cart.Data;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

    public class CartDataDeserializer implements JsonDeserializer<Data> {

    @Override
    public Data deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final Data data = new Data();
        final List<CartItemData> cartItemDataList = new ArrayList<>();
        final List<JsonObject> jsonObjects = getDataWithKey(jsonElement.getAsJsonObject(), "item_");
        for (final JsonObject jsonObject : jsonObjects) {
            final CartItemData cartItemData = new CartItemData();
            cartItemData.setId(jsonObject.get("id").getAsString());
            cartItemData.setTag(jsonObject.get("tag").getAsString());
            cartItemData.setType(jsonObject.get("type").getAsString());
            final CartItemDataFields cartItemDataFields = new CartItemDataFields();
            cartItemDataFields.setCartItemId(jsonObject.getAsJsonObject("fields").get("cartItemId").getAsString());
            cartItemDataFields.setOperation("delete");
            cartItemData.setFields(cartItemDataFields);
            cartItemDataList.add(cartItemData);
        }
        data.setCartItems(cartItemDataList);
        return data;
    }

    private List<JsonObject> getDataWithKey(final JsonObject dataObject, final String prefix) {
        final List<JsonObject> jsonObjects = new ArrayList<>();
        for (final String key : dataObject.keySet()) {
            if (key.startsWith(prefix)) {
                jsonObjects.add(dataObject.getAsJsonObject(key));
            }
        }
        return jsonObjects;
    }
}
