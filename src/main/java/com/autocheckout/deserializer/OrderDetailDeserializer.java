package com.autocheckout.deserializer;

import java.lang.reflect.Type;

import com.autocheckout.data.lazada.orderdetail.OrderDetail;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class OrderDetailDeserializer implements JsonDeserializer<OrderDetail> {

    @Override
    public OrderDetail deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final OrderDetail orderDetail = new OrderDetail();
        final JsonObject data = jsonElement.getAsJsonObject().getAsJsonObject("data");
        final JsonObject deliveryAddress = getDataWithKey(data, "deliverySummary", "address");
        final JsonObject detailInfo = getDataWithKey(data, "detailInfo", null);
        final JsonObject login = getDataWithKey(data, "login", null);
        final JsonObject orderItem = getDataWithKey(data, "orderItem", null);
        final JsonObject shippingInfo = getDataWithKey(data, "shippingInfo", null);
        final JsonObject totalSummary = getDataWithKey(data, "totalSummary", null);

        orderDetail.setStatus(getData(shippingInfo, "fields", "statusMap", "active").getAsString());
        orderDetail.setOrderId(getData(detailInfo, "id").getAsString());
        orderDetail.setBuyerName(getData(login, "fields", "buyerName").getAsString());
        orderDetail.setBuyerEmail(getData(login, "fields", "buyerEmail").getAsString());
        orderDetail.setPhone(getData(deliveryAddress, "fields", "phone").getAsString());
        final String address = getData(deliveryAddress, "fields", "address").getAsString();
        orderDetail.setRegion(address.substring(address.indexOf(',') + 1));
        orderDetail.setAddress(address);
        orderDetail.setCreateAt(getData(detailInfo, "fields", "createdAt").getAsString());
        orderDetail.setDeliveryDate(getData(shippingInfo, "fields", "delivery", "createdAt").getAsString());
        orderDetail.setProductName(getData(orderItem, "fields", "title").getAsString());
        orderDetail.setProductLink(getData(orderItem, "fields", "itemUrl").getAsString());
        orderDetail.setPrice(getData(totalSummary, "fields", "total").getAsString());
        final JsonElement trackingList = getData(shippingInfo, "fields", "trackingList");
        if (!trackingList.isJsonArray()
          || trackingList.getAsJsonArray().size() == 0
            || !trackingList.getAsJsonArray().get(0).getAsJsonObject().has("info")) {
            throw new JsonParseException("Invalid order detail data");
        }
        final JsonArray trackingListArray = trackingList.getAsJsonArray();
        orderDetail.setNote(trackingListArray.get(0).getAsJsonObject().get("info").getAsString());
        boolean hasTrackingNumber = false;
        for (int i = 0; i < trackingListArray.size(); ++i) {
            final String content = trackingListArray.get(i).getAsJsonObject().get("info").getAsString();
            if (content.contains("mã vận đơn")) {
                orderDetail.setLogisticPartner(content.substring(content.indexOf("Lazada: ") + 8, content.indexOf(" với")));
                orderDetail.setTrackingNumber(content.substring(content.indexOf("đơn ") + 4, content.indexOf(".")));
                hasTrackingNumber = true;
                break;
            }
        }
        if (!hasTrackingNumber) {
            orderDetail.setLogisticPartner("Trống");
            orderDetail.setTrackingNumber("Trống");
        }
        return orderDetail;
    }

    private JsonElement getData(final JsonObject dataObject, final String... memberNames) {
        JsonObject intermediateNode = dataObject;
        for (int i = 0; i < memberNames.length - 1; ++i) {
            if (!intermediateNode.has(memberNames[i])) {
                throw new JsonParseException("Invalid order detail data");
            }
            intermediateNode = intermediateNode.getAsJsonObject(memberNames[i]);
        }
        if (!intermediateNode.has(memberNames[memberNames.length - 1])) {
            throw new JsonParseException("Invalid order detail data");
        }
        return intermediateNode.get(memberNames[memberNames.length - 1]);
    }

    private JsonObject getDataWithKey(final JsonObject dataObject, final String prefix, final String postFix) {
        for (final String key : dataObject.keySet()) {
            if (key.startsWith(prefix)) {
                if (postFix == null) {
                    return dataObject.getAsJsonObject(key);
                }
                if (key.endsWith(postFix)) {
                    return dataObject.getAsJsonObject(key);
                }
            }
        }
        throw new JsonParseException("Invalid order detail data");
    }
}
