package com.autocheckout.deserializer;

import java.lang.reflect.Type;

import com.autocheckout.data.lazada.account.AddressInfo;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class AddressInfoDeserializer implements JsonDeserializer<AddressInfo> {

    @Override
    public AddressInfo deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final AddressInfo addressInfo = new AddressInfo();
        final JsonObject addressData = jsonElement.getAsJsonObject().getAsJsonArray("module").get(0).getAsJsonObject();
        addressInfo.setAddressId(addressData.get("id").getAsString());
        addressInfo.setName(addressData.get("name").getAsString());
        addressInfo.setPhone(addressData.get("phone").getAsString());
        return addressInfo;
    }
}
