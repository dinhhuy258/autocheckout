package com.autocheckout.util;

import java.util.ArrayList;
import java.util.Collection;

public class CircularList<E> extends ArrayList<E> {

    public CircularList(final Collection<? extends E> c) {
        super(c);
    }

    public CircularList() {
        super();
    }

    @Override
    public E get(final int index) {
        if (size() == 0) {
            throw new NullPointerException("CircularList is empty");
        }
        return super.get(index % size());
    }
}
