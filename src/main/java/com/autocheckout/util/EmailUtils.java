package com.autocheckout.util;

import com.autocheckout.LOG;
import com.autocheckout.constant.Constants;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public final class EmailUtils {

    private static final String SMTP_HOST = "smtp.gmail.com";

    private static final String SMTP_PORT = "587";

    private static String hostName = "Uknown Host";

    static {
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            LOG.debug("Error when identify who you are :" + ex.getMessage());
        }
        LOG.debug(hostName);
    }

    private EmailUtils() {
    }

    public static void sendMail(final String subject, final String content, final String... attachments) {
        LOG.debug("Create report...");
        // Sender's email
        final String username = Constants.MAIL.USER_NAME;
        final String password = Constants.MAIL.PASSWORD;
        // Recipient' email
        final String toEmail = Constants.MAIL.USER_NAME;

        // Setup mail server properties
        final Properties properties = System.getProperties();
        properties.put("mail.smtp.host", SMTP_HOST);
        properties.put("mail.smtp.port", SMTP_PORT);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.user", username);
        properties.put("mail.smtp.password", password);

        // creates a new session with an authenticator
        final Authenticator authenticator = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
        // Get the default Session object.
        final Session session = Session.getDefaultInstance(properties, authenticator);
        // Creates a new e-mail message
        final Message message = new MimeMessage(session);
        // Set From: header field of the header.
        try {
            final InternetAddress fromAddress = new InternetAddress(username);
            message.setFrom(fromAddress);
            // Set To: header field of the header.
            final InternetAddress[] toAddresses = {new InternetAddress(toEmail)};
            message.setRecipients(Message.RecipientType.TO, toAddresses);
            // Set Subject: header field
            message.setSubject(String.join(" | ", hostName, subject));
            // Set the sent date
            message.setSentDate(new Date());
            // Create multi part
            final Multipart multipart = new MimeMultipart();
            // Creates body part for the message
            final MimeBodyPart messageBodyPart = new MimeBodyPart();
            String messageContent = content;
            // Add attachments
            if (attachments != null && attachments.length > 0) {
                for (final String attachment : attachments) {
                    final MimeBodyPart attachPart = new MimeBodyPart();
                    try {
                        final File file = new File(attachment);
                        if (file.exists() && !file.isDirectory()) {
                            attachPart.attachFile(attachment);
                            multipart.addBodyPart(attachPart);
                        } else {
                            messageContent = messageContent + ". " + "File is not exist: " + attachment;
                            LOG.debug("File is not exist: " + attachment);
                        }
                    } catch (final IOException e) {
                        messageContent = messageContent + ". " + "Can not attach file: " + attachment;
                        LOG.debug("Can not attach file: " + attachment);
                    }
                }
            }
            messageBodyPart.setContent(messageContent, "text/html");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            // Send message
            Transport.send(message);
            LOG.debug("Create report done!!!");
        } catch (final MessagingException exception) {
            LOG.debug("There is an error while create report!!!");
            exception.printStackTrace();
        }
    }
}
