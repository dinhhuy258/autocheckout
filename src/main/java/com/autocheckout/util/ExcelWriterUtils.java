package com.autocheckout.util;

import com.autocheckout.LOG;
import com.autocheckout.data.lazada.orderdetail.OrderDetail;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public final class ExcelWriterUtils {

    private ExcelWriterUtils() {
    }

    public static void exportOrderDetailListToExcel(final List<OrderDetail> orderDetails, final String outputFolder) throws IOException {
        final String[] columns = {"Trạng thái", "Mã đơn hàng", "Người đặt", "Email", "SĐT", "Khu Vực", "Địa chỉ", "Ngày đặt", "Ngày nhận", "Tên sản phẩm", "Link sản phẩm", "Giá", "Nội dung", "Giao nhận", "Mã giao nhận"};
        // Create a workbook
        LOG.debug("com.autocheckout.util.ExcelWriterUtils.exportOrderDetailListToExcel()");
        final Workbook workbook = new HSSFWorkbook();
        LOG.debug("Create Folder * HSSFWorkbook");
        // Create a sheet
        final Sheet sheet = workbook.createSheet("Thống kê");
        LOG.debug("Create Folder * createSheet");
        // Create a Row
        final Row headerRow = sheet.createRow(0);
        LOG.debug("Create Folder * createRow");
        // Creating cells
        for (int i = 0; i < columns.length; i++) {
            final Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
        }
        LOG.debug("Create Folder * createCell");
        // Create Other rows and cells with another data
        int rowNum = 1;
        for (final OrderDetail orderDetail : orderDetails) {
            final Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(orderDetail.getStatus());
            row.createCell(1).setCellValue(orderDetail.getOrderId());
            row.createCell(2).setCellValue(orderDetail.getBuyerName());
            row.createCell(3).setCellValue(orderDetail.getBuyerEmail());
            row.createCell(4).setCellValue(orderDetail.getPhone());
            row.createCell(5).setCellValue(orderDetail.getRegion());
            row.createCell(6).setCellValue(orderDetail.getAddress());
            row.createCell(7).setCellValue(orderDetail.getCreateAt());
            row.createCell(8).setCellValue(orderDetail.getDeliveryDate());
            row.createCell(9).setCellValue(orderDetail.getProductName());
            row.createCell(10).setCellValue(orderDetail.getProductLink());
            row.createCell(11).setCellValue(orderDetail.getPrice());
            row.createCell(12).setCellValue(orderDetail.getNote());
            row.createCell(13).setCellValue(orderDetail.getLogisticPartner());
            row.createCell(14).setCellValue(orderDetail.getTrackingNumber());
        }
        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
        // Write the output to a file
        final File file = new File(outputFolder);
        LOG.debug("Create Folder *" + file.getAbsolutePath());
        if (!file.exists()) {
            LOG.debug("Create Folder *" + file.getAbsolutePath());
            file.getParentFile().mkdirs();
            file.createNewFile();
        }

        final FileOutputStream fileOut = new FileOutputStream(outputFolder);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }
}
