/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.util;

import com.autocheckout.LOG;
import com.autocheckout.constant.Constants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.jsoup.helper.StringUtil;

/**
 *
 * @author anhdtc-jp
 */
public class FileUtils {

    public static ArrayList<String> importSelectedFile(ArrayList<File> fileList) {
        ArrayList<String> itemList = new ArrayList<>();
        BufferedReader br = null;
        try {
            for (File file : fileList) {
                if (!file.exists()) {
                    continue;
                }
                br = new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream(file), "UTF8"));
                // Skip first line (Regex of file: Name / Email Name)
                br.readLine();
                String currentLine;
                while (((currentLine = br.readLine()) != null) && (!StringUtil.isBlank(currentLine))) {
                    itemList.add(currentLine);
                }
            }

        } catch (final IOException e) {
            LOG.debug("Read File Error:  " + e.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (final IOException e) {
                LOG.debug("Error when close File Reader :" + e.getMessage());
            }
        }
        return itemList;
    }

    /**
     * Import data from file to list string.
     *
     * @param name File Location.
     * @return
     */
    public static ArrayList<String> importFile(final String name) {
        final String fileName = System.getProperty("user.dir") + Constants.FILE_PATH.RESOURCES_PATH
            + name;
        return importFileFullPath(fileName);
    }

    /**
     * Import data from file to list string.
     *
     * @param fileName File Location (Absolute Path).
     * @return
     */
    public static ArrayList<String> importFileFullPath(final String fileName) {
        ArrayList<String> itemList = new ArrayList<>();
        BufferedReader br = null;
        FileReader fr = null;
        try {
            final File file = new File(fileName);
            if (!file.exists()) {
                file.createNewFile();
            }

            br = new BufferedReader(
                new InputStreamReader(
                    new FileInputStream(file), "UTF8"));
            // Skip first line (Regex of file: Name / Email Name)
            br.readLine();

            String currentLine;

            while (((currentLine = br.readLine()) != null) && (!StringUtil.isBlank(currentLine))) {
                itemList.add(currentLine);
            }
        } catch (final IOException e) {
            LOG.debug("Read File Error: " + fileName + " : " + e.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (fr != null) {
                    fr.close();
                }
            } catch (final IOException e) {
                LOG.debug("Error when close File Reader :" + e.getMessage());
            }
        }
        return itemList;
    }

    /**
     * Export Text Data to file.
     *
     * @param name File name.
     * @param text Text data.
     */
    public static void exportToFile(final String name, final String text) {
        final String fileName = System.getProperty("user.dir") + Constants.FILE_PATH.RESOURCES_PATH
            + name;
        exportToFileFullPath(fileName, text);
    }

    /**
     * Export Text Data to file.
     *
     * @param fileName File name ( Absolute Path)
     * @param text Text data.
     */
    public static void exportToFileFullPath(final String fileName, final String text) {
        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            final File file = new File(fileName);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
                fw = new FileWriter(fileName);
                fw.write("Output File" + '\n');

            } else {
                fw = new FileWriter(fileName, true);
            }
            bw = new BufferedWriter(fw);
            bw.write(text + '\n');
        } catch (final IOException e) {
            LOG.debug(e);
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (final IOException e) {
                LOG.debug("Error In File Reader :" + e.getMessage());
            }
        }
    }

    public static void makeDir(String folderPath) {

        final File file = new File(folderPath);

        if (!file.exists()) {

            if (file.isDirectory()) {
                file.mkdirs();
            } else {
                file.getParentFile().mkdirs();
            }
        }
    }

}
