package com.autocheckout.util;

import com.autocheckout.LOG;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.Normalizer;
import java.util.regex.Pattern;

public final class StringUtils {

    public static String convertToLatin(final String str) {
        try {
            final String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
            final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
            final String al = pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
            LOG.debug(str + " - " + al);
            return al;
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isUrlEncoded(final String str) {
        String decode = null;
        try {
            decode = URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        return !str.equalsIgnoreCase(decode);
    }

    public static boolean isNumberFormat(String input) {
        String patternStr = "^[0-9]*$";

        return !isBlank(input) && input.matches(patternStr);
    }

    public static boolean isBlank(String input) {
        return input == null || input.replaceAll(" ", "").length() == 0;
    }
}
