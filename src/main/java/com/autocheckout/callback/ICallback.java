/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.callback;

import com.autocheckout.executor.task.TaskResult;

/**
 *
 * @author anhdtc-jp
 * @param <T>
 */
public interface ICallback<T extends TaskResult> {

    public void callBack(T result);

    public void callBackWhenDone(String message);
}
