/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.callback;

/**
 *
 * @author anhdtc-jp
 */
public interface AutoBuyCallBack {

    public void updateStatus(String productLink, String email, String orderId, boolean isSuccess);
}
