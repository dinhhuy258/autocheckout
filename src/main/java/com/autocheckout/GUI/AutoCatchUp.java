/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.GUI;

import com.autocheckout.LOG;
import com.autocheckout.callback.ICallback;
import com.autocheckout.constant.Constants;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.account.Account;
import com.autocheckout.data.lazada.account.Address;
import com.autocheckout.data.lazada.account.FullName;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.task.lazada.checkout.CheckoutTaskDispatcher;
import com.autocheckout.executor.task.lazada.checkout.CheckoutTaskResult;
import com.autocheckout.service.GenerateAddressService;
import com.autocheckout.service.GenerateNameService;
import com.autocheckout.service.GeneratePhoneNumberService;
import com.autocheckout.service.affiliate.AffiliateService;
import com.autocheckout.util.CircularList;
import com.autocheckout.util.FileUtils;
import com.autocheckout.util.StringUtils;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import org.apache.commons.collections4.CollectionUtils;

/**
 *
 * @author anhdtc-jp
 */
public class AutoCatchUp extends javax.swing.JFrame implements ICallback<CheckoutTaskResult> {

    private final CircularList<Account> accountList;
    private ArrayList<Address> cityList;
    private ArrayList<Address> prefectureList;
    private ArrayList<Address> townList;
    private ArrayList<String> phoneList;
    private final ArrayList<Catching> catchingUI;

    private CircularList<String> proxies;

    private final JFrame parentFrame;

    private final DefaultTableModel model;

    private String exportFilePath;
    private String addressPath;

    private int index = 1;
    private int tmpIndex = 0;
    private int success = 0;
    private int fails = 0;
    private int wattingTime = 0;

    private final AffiliateService affiliateService;

    boolean isCancelCheck = false;

    /**
     * Creates new form LazadaAutoBuy
     *
     * @param parentFrame
     * @param proxies
     */
    public AutoCatchUp(final JFrame parentFrame, final CircularList<String> proxies) {
        affiliateService = new AffiliateService();
        initComponents();
        initItemForCBBTinh();
        this.proxies = proxies;
        this.parentFrame = parentFrame;
        accountList = new CircularList<>();
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        final TableColumnModel columnModel = tblImportedAccount.getColumnModel();

        columnModel.getColumn(0).setPreferredWidth(150);
        columnModel.getColumn(1).setPreferredWidth(50);

        phoneList = GeneratePhoneNumberService.getPHONE_BOOK();

        txtPhoneNumber.setText("");
        for (final String phoneNumber : phoneList) {
            txtPhoneNumber.append(phoneNumber + "\n");
        }

        catchingUI = new ArrayList<>();

        txtProxy.setText("");
        for (final String proxy : proxies) {
            txtProxy.append(proxy + "\n");
        }

        model = (DefaultTableModel) tblResult.getModel();

        final TableColumnModel resultTableColumnModel = tblResult.getColumnModel();

        resultTableColumnModel.getColumn(0).setPreferredWidth(10);
        resultTableColumnModel.getColumn(1).setPreferredWidth(10);
        resultTableColumnModel.getColumn(2).setPreferredWidth(150);
        resultTableColumnModel.getColumn(3).setPreferredWidth(150);
        resultTableColumnModel.getColumn(4).setPreferredWidth(55);

        inportAccountList(System.getProperty("user.dir")
            + Constants.FILE_PATH.RESOURCES_PATH + LazadaConstant.FILE_PATH.DEFAULT_ACCOUNT_PATH, false);

        exportFilePath = System.getProperty("user.dir")
            + LazadaConstant.FILE_PATH.ORDER_TEMPORADY;

        txtExportFilePath.setText(exportFilePath);

        txtNumOfThread.setText(Integer.toString(Constants.DEFAULT.NUMBER_OF_THREAD));
    }

    private void inportAccountList(final String accountFileFullPath, final boolean isAppend) {
        LOG.debug("com.autocheckout.GUI.AutoBuy.inportAccountList()");
        // Get file name
        final String[] split = accountFileFullPath.split("/");
        LOG.debug(Arrays.toString(split) + " Tài khoản");

        txtAccountFileName.setText(split[split.length - 1]);
        txtAccountFileName.setToolTipText(accountFileFullPath);

        final ArrayList<String> accountStrList = FileUtils.importFileFullPath(accountFileFullPath);

        final DefaultTableModel importAccountTable = (DefaultTableModel) tblImportedAccount.getModel();
        if (!isAppend) {
            importAccountTable.getDataVector().removeAllElements();
            accountList.clear();
        }

        for (final String accountItemStr : accountStrList) {
            final String[] splitedItem = accountItemStr.split(Constants.SYMBOL.SPLIT_STRING_SYMBOL);
            importAccountTable.addRow(splitedItem);
            if (splitedItem.length >= 2) {
                accountList.add(new Account(splitedItem[0], splitedItem[1]));
            }
        }
        lblSoLuongAcc.setText(String.format("%d Tài khoản", accountList.size()));
        LOG.debug(String.format("%d Tài khoản", accountList.size()));
    }

    private void initItemForCBBTinh() {

        cityList = GenerateAddressService.getCityList();
        cbbTinh.removeAllItems();
        cbbTinh.addItem("Please select an item below.");
        final int cityListSize = cityList.size();
        for (int i = 0; i < cityListSize; i++) {
            final Address item = cityList.get(i);
            cbbTinh.addItem(item.getDisplayName());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
     * content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        lblLink = new javax.swing.JLabel();
        lblSoLuong = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtVoucher = new javax.swing.JTextField();
        txtNumOfOrder = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        rdAccountAddress = new javax.swing.JRadioButton();
        rdEditAddress = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        txtAccountFileName = new javax.swing.JTextField();
        btnImportAcc = new javax.swing.JButton();
        btnStartAutoBuy = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblImportedAccount = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cbbTinh = new javax.swing.JComboBox<>();
        cbbQuanHuyen = new javax.swing.JComboBox<>();
        cbbPhuongXa = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        txtNumOfThread = new javax.swing.JTextField();
        lblThreadError = new javax.swing.JLabel();
        lblSoLuongError = new javax.swing.JLabel();
        lblError = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtProxy = new javax.swing.JTextArea();
        lblProxy = new javax.swing.JLabel();
        btnImportProxy = new javax.swing.JButton();
        ckbMultiProductAllow = new javax.swing.JCheckBox();
        jLabel10 = new javax.swing.JLabel();
        btnImportPhone = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtPhoneNumber = new javax.swing.JTextArea();
        lblPhoneBookName = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblResult = new javax.swing.JTable();
        prbStatus = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();
        btnAddAcc = new javax.swing.JButton();
        lblSoLuongAcc = new javax.swing.JLabel();
        txtProductLink = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        btnExportFileChosse = new javax.swing.JButton();
        txtExportFilePath = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtDelay = new javax.swing.JTextField();
        lblWait = new javax.swing.JLabel();

        setTitle("Canh me mua hàng");
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(final java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        lblLink.setText("Link sản phẩm");

        lblSoLuong.setText("Số lượng");
        lblSoLuong.setToolTipText("");

        jLabel3.setText("Mã giảm giá  ");

        txtVoucher.setToolTipText("Mã giảm giá");

        txtNumOfOrder.setText("0");
        txtNumOfOrder.setToolTipText("Số lượng");

        jLabel4.setText("Địa chỉ ");
        jLabel4.setToolTipText("");

        buttonGroup1.add(rdAccountAddress);
        rdAccountAddress.setText("Địa chỉ trong tài khoản");
        rdAccountAddress.setToolTipText("");
        rdAccountAddress.addChangeListener(new javax.swing.event.ChangeListener() {
            @Override
            public void stateChanged(final javax.swing.event.ChangeEvent evt) {
                rdAccountAddressStateChanged(evt);
            }
        });

        buttonGroup1.add(rdEditAddress);
        rdEditAddress.setSelected(true);
        rdEditAddress.setText("Tuỳ chỉnh");

        jLabel5.setText("Account");

        txtAccountFileName.setToolTipText("Đường dẫn đến file account");

        btnImportAcc.setText("Nhập mới");
        btnImportAcc.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                btnImportAccActionPerformed(evt);
            }
        });

        btnStartAutoBuy.setText("Canh mặt hàng này");
        btnStartAutoBuy.setActionCommand("Bắt đầu canh me");
        btnStartAutoBuy.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                btnStartAutoBuyActionPerformed(evt);
            }
        });

        tblImportedAccount.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Email", "Password"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            @Override
            public Class getColumnClass(final int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(final int rowIndex, final int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblImportedAccount);

        jLabel6.setText("Tỉnh");
        jLabel6.setToolTipText("");

        jLabel7.setText("Quận");
        jLabel7.setToolTipText("");

        jLabel8.setText("Phường");

        cbbTinh.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbbTinh.setToolTipText("Tỉnh / Thành Phố");
        cbbTinh.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                cbbTinhActionPerformed(evt);
            }
        });

        cbbQuanHuyen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbbQuanHuyen.setToolTipText("Quận / Huyện");
        cbbQuanHuyen.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                cbbQuanHuyenActionPerformed(evt);
            }
        });

        cbbPhuongXa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbbPhuongXa.setToolTipText("Xã/Phường");

        jLabel9.setText("Threads :");

        txtNumOfThread.setText("50");
        txtNumOfThread.setToolTipText("Số thread bạn muốn sử dụng.");

        lblThreadError.setForeground(new java.awt.Color(255, 0, 0));

        lblSoLuongError.setForeground(new java.awt.Color(255, 0, 0));

        lblError.setForeground(new java.awt.Color(255, 0, 0));

        jLabel1.setText("Proxy :");

        txtProxy.setEditable(false);
        txtProxy.setColumns(20);
        txtProxy.setRows(5);
        jScrollPane1.setViewportView(txtProxy);

        lblProxy.setText("Default proxy");

        btnImportProxy.setText("Import");
        btnImportProxy.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                btnImportProxyActionPerformed(evt);
            }
        });

        ckbMultiProductAllow.setSelected(true);
        ckbMultiProductAllow.setText("1 account có thể mua nhiều đơn hàng.");

        jLabel10.setText("SDT");
        jLabel10.setToolTipText("");

        btnImportPhone.setText("Import");
        btnImportPhone.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                btnImportPhoneActionPerformed(evt);
            }
        });

        txtPhoneNumber.setEditable(false);
        txtPhoneNumber.setColumns(20);
        txtPhoneNumber.setRows(5);
        jScrollPane3.setViewportView(txtPhoneNumber);

        lblPhoneBookName.setText("Danh sách mặc định");

        tblResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "STT", "Thành công", "Email", "Mã đơn hàng", "Giá tiền"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            @Override
            public Class getColumnClass(final int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(final int rowIndex, final int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane4.setViewportView(tblResult);

        jLabel2.setText("0 thành công / 0 thất bại");

        btnAddAcc.setText("Thêm tài khoản");
        btnAddAcc.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                btnAddAccActionPerformed(evt);
            }
        });

        lblSoLuongAcc.setText("11 tài khoản");

        jLabel11.setText("File report");

        btnExportFileChosse.setText("Chọn nơi lưu");
        btnExportFileChosse.setToolTipText("");
        btnExportFileChosse.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                btnExportFileChosseActionPerformed(evt);
            }
        });

        txtExportFilePath.setEditable(false);

        jLabel12.setText("Delay (Giây)");

        txtDelay.setText("30");

        lblWait.setText("Waiting");

        final javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(prbStatus, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane4)
                            .addComponent(btnStartAutoBuy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(32, 32, 32))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtVoucher, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(54, 54, 54)
                                        .addComponent(rdAccountAddress)
                                        .addGap(18, 18, 18)
                                        .addComponent(rdEditAddress))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabel7)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel10))
                                        .addGap(53, 53, 53)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblPhoneBookName)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(btnImportPhone))
                                            .addComponent(cbbQuanHuyen, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cbbTinh, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cbbPhuongXa, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                .addComponent(jLabel12)
                                                .addGap(27, 27, 27)
                                                .addComponent(txtDelay))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                .addComponent(lblSoLuong)
                                                .addGap(45, 45, 45)
                                                .addComponent(txtNumOfOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblWait)))
                                .addGap(2, 2, 2)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblSoLuongAcc)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtNumOfThread, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 438, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(31, 31, 31)
                                        .addComponent(lblProxy)
                                        .addGap(46, 46, 46)
                                        .addComponent(btnImportProxy))
                                    .addComponent(ckbMultiProductAllow)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnExportFileChosse))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(0, 0, 0)
                                        .addComponent(txtAccountFileName, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(btnImportAcc)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnAddAcc, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtExportFilePath, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblLink)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtProductLink, javax.swing.GroupLayout.PREFERRED_SIZE, 745, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblSoLuongError)
                                .addGap(6, 6, 6)
                                .addComponent(lblThreadError)))
                        .addGap(0, 41, Short.MAX_VALUE)))
                .addComponent(lblError)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(365, 365, 365)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblThreadError)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSoLuongError)
                                .addGap(25, 25, 25)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel11)
                                    .addComponent(btnExportFileChosse))
                                .addGap(10, 10, 10)
                                .addComponent(txtExportFilePath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(txtAccountFileName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnImportAcc)
                                    .addComponent(btnAddAcc))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblSoLuongAcc)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ckbMultiProductAllow)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(lblProxy)
                                    .addComponent(btnImportProxy))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(txtNumOfThread, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)))
                        .addComponent(btnStartAutoBuy)
                        .addGap(18, 18, 18))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblLink)
                            .addComponent(txtProductLink, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblWait))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSoLuong)
                            .addComponent(txtNumOfOrder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtVoucher, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(rdAccountAddress)
                            .addComponent(rdEditAddress))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(cbbTinh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(cbbQuanHuyen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(cbbPhuongXa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(btnImportPhone)
                            .addComponent(lblPhoneBookName))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblError)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartAutoBuyActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartAutoBuyActionPerformed

        // TODO add your handling code here:
        final String productUrl = txtProductLink.getText();

        if (!affiliateService.isAffUrl(productUrl)) {
            txtProductLink.setBackground(Color.red);
            JOptionPane.showConfirmDialog(this, "Link san pham phai tu cac trang quang cap moi duoc. \n"
                + "Phai chua cac tu fast.accesstrade  go.ecotrackings go.masoffer");
            return;
        } else {
            txtProductLink.setBackground(Color.white);
        }

        if (!StringUtils.isNumberFormat(txtNumOfOrder.getText())) {
            txtNumOfOrder.setBackground(Color.red);
            lblSoLuongError.setText("Input number only please.");
            return;
        } else {
            txtNumOfOrder.setBackground(Color.white);
            lblSoLuongError.setText("");
        }

        if (!StringUtils.isNumberFormat(txtNumOfThread.getText())) {
            txtNumOfThread.setBackground(Color.red);
            lblThreadError.setText("Input number only please.");
            return;
        } else {
            txtNumOfThread.setBackground(Color.white);
            lblThreadError.setText("");
        }

        if (accountList.size() <= 0) {
            txtAccountFileName.setBackground(Color.red);
            return;
        } else {
            txtAccountFileName.setBackground(Color.white);
        }

        if (StringUtils.isBlank(exportFilePath)) {
            txtExportFilePath.setBackground(Color.red);
            btnExportFileChosse.setForeground(Color.red);
            return;
        } else {
            txtExportFilePath.setBackground(Color.white);
            btnExportFileChosse.setForeground(Color.black);
        }

        final int numOfThread = Integer.parseInt(txtNumOfThread.getText());

        final int numOfOrders = Integer.parseInt(txtNumOfOrder.getText());

        if (numOfOrders < 1) {
            JOptionPane.showMessageDialog(this, "Số đơn hàng phải lớn hơn 0", "Nhập sai số đơn hàng",
                JOptionPane.WARNING_MESSAGE);

            txtNumOfOrder.setBackground(Color.red);
            return;
        } else {
            txtNumOfOrder.setBackground(Color.white);
        }

        final int soAcc = accountList.size();

        if (numOfOrders > soAcc && !ckbMultiProductAllow.isSelected()) {

            JOptionPane.showMessageDialog(this, "Số lượng account đang ít hơn số đơn hàng cần đặt\n"
                + "vui lòng import thêm account hoặc cho phép 1 account đặt nhiều đơn", "Thiếu account rồi",
                JOptionPane.WARNING_MESSAGE);

            ckbMultiProductAllow.setForeground(Color.red);
            return;
        } else {
            ckbMultiProductAllow.setForeground(Color.black);
        }

        final ArrayList<SubmitAddressInfo> newAddressList;

        if (rdEditAddress.isSelected()) {
            newAddressList = generateSubmitAddressList(numOfOrders);
        } else {
            newAddressList = null;
        }

        final String delayStr = txtDelay.getText();

        final int delayTmp;

        if (StringUtils.isBlank(delayStr) || !StringUtils.isNumberFormat(delayStr)) {

            JOptionPane.showConfirmDialog(this, "Hay nhap thoi gian cho hon 5 giay");
            return;
        } else {

            delayTmp = Integer.parseInt(txtDelay.getText());
            if (delayTmp < 5) {
                JOptionPane.showConfirmDialog(this, "Hay nhap thoi gian cho hon 5 giay");
                return;
            }
        }

        final int delayTime = delayTmp;

        final String voucherCode;
        voucherCode = txtVoucher.getText();
        btnStartAutoBuy.setEnabled(false);

        final Catching newCatching = new Catching(parentFrame, numOfThread, exportFilePath, accountList, productUrl,
            numOfOrders, voucherCode, newAddressList, proxies, addressPath, delayTime);
        newCatching.setVisible(true);
        catchingUI.add(newCatching);

        try {
            Thread.sleep(2000);
        } catch (final InterruptedException ex) {
            Logger.getLogger(AutoCatchUp.class.getName()).log(Level.SEVERE, null, ex);
        }
        btnStartAutoBuy.setEnabled(true);
        tmpIndex = 1;
        prbStatus.setMaximum(numOfOrders);

    }//GEN-LAST:event_btnStartAutoBuyActionPerformed

    private void rdAccountAddressStateChanged(final javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_rdAccountAddressStateChanged
        // TODO add your handling code here:
        final boolean isUseCustomAddress = !rdAccountAddress.isSelected();
        cbbTinh.setEnabled(isUseCustomAddress);
        cbbQuanHuyen.setEnabled(isUseCustomAddress);
        cbbPhuongXa.setEnabled(isUseCustomAddress);
    }//GEN-LAST:event_rdAccountAddressStateChanged

    private void btnImportAccActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportAccActionPerformed
        // TODO add your handling code here:
        final JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir") + "/");
        final int result = fileChooser.showOpenDialog(this);
        final String fileNameFull;

        if (result == JFileChooser.APPROVE_OPTION) {

            fileNameFull = fileChooser.getSelectedFile().getAbsolutePath();
            inportAccountList(fileNameFull, false);
        }
    }//GEN-LAST:event_btnImportAccActionPerformed

    private void cbbTinhActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbTinhActionPerformed
        // TODO add your handling code here:
        cbbQuanHuyen.removeAllItems();
        cbbQuanHuyen.addItem("Please select an item below.");

        if (cbbTinh.getSelectedIndex() < 1) {

            return;
        }

        prefectureList = GenerateAddressService.getListPrefectureOfCity(cityList.get(cbbTinh.getSelectedIndex() - 1));

        final int prefectureListSize = prefectureList.size();
        for (int i = 0; i < prefectureListSize; i++) {
            final Address item = prefectureList.get(i);
            cbbQuanHuyen.addItem(item.getDisplayName());
        }
    }//GEN-LAST:event_cbbTinhActionPerformed

    private void cbbQuanHuyenActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbQuanHuyenActionPerformed

        LOG.debug("com.autocheckout.GUI.AutoRegAcc.cbbQuanHuyenItemStateChanged()");
        cbbPhuongXa.removeAllItems();
        cbbPhuongXa.addItem("Please select an item below.");

        if (cbbQuanHuyen.getSelectedIndex() < 1) {
            return;
        }

        townList = GenerateAddressService.getListTownOfPrefecture(prefectureList.get(cbbQuanHuyen.getSelectedIndex() - 1));
        final int townListSize = townList.size();
        for (int i = 0; i < townListSize; i++) {
            final Address item = townList.get(i);
            cbbPhuongXa.addItem(item.getDisplayName());
        }
        LOG.debug("CBB Value is " + cbbQuanHuyen.getSelectedItem());
        LOG.debug("City Value is " + prefectureList.get(cbbQuanHuyen.getSelectedIndex() - 1).getAddressName());
    }//GEN-LAST:event_cbbQuanHuyenActionPerformed

    private void btnImportProxyActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportProxyActionPerformed
        // TODO add your handling code here:
        final JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir") + "/");

        final int result = fileChooser.showOpenDialog(this);

        final String fileName;
        final String fileNameFull;
        if (result == JFileChooser.APPROVE_OPTION) {

            fileName = fileChooser.getSelectedFile().getName();
            lblProxy.setText(fileName);

            fileNameFull = fileChooser.getSelectedFile().getAbsolutePath();
            proxies = new CircularList<>(FileUtils.importFileFullPath(fileNameFull));
            txtProxy.setText("");
            for (final String proxy : proxies) {
                txtProxy.append(proxy + "\n");
            }
        }
    }//GEN-LAST:event_btnImportProxyActionPerformed

    private void formWindowClosing(final java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        parentFrame.setVisible(true);
    }//GEN-LAST:event_formWindowClosing

    private void btnImportPhoneActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportPhoneActionPerformed
        // TODO add your handling code here:
        final JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir") + "/");

        final int result = fileChooser.showOpenDialog(this);

        final String fileName;
        final String fileNameFull;
        if (result == JFileChooser.APPROVE_OPTION) {

            fileName = fileChooser.getSelectedFile().getName();
            lblPhoneBookName.setText(fileName);

            fileNameFull = fileChooser.getSelectedFile().getAbsolutePath();
            phoneList = FileUtils.importFileFullPath(fileNameFull);

            if (CollectionUtils.isNotEmpty(phoneList)) {
                txtPhoneNumber.setText("");

                if (!GeneratePhoneNumberService.setPHONE_BOOK(phoneList)) {
                    JOptionPane.showConfirmDialog(this, "Vui long kiem tra lai file data so dien thoai.");
                }

                lblPhoneBookName.setForeground(Color.black);

                for (final String phoneNumber : phoneList) {
                    txtPhoneNumber.append(phoneNumber + "\n");
                }
            } else {
                lblPhoneBookName.setText("Vui long kiem tra lai file data so dien thoai.");
                lblPhoneBookName.setForeground(Color.red);
            }
        }
    }//GEN-LAST:event_btnImportPhoneActionPerformed

    private void btnAddAccActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddAccActionPerformed
        // TODO add your handling code here:
        final JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir") + "/");

        final int result = fileChooser.showOpenDialog(this);

        final String fileNameFull;

        if (result == JFileChooser.APPROVE_OPTION) {

            fileNameFull = fileChooser.getSelectedFile().getAbsolutePath();
            inportAccountList(fileNameFull, true);
        }
    }//GEN-LAST:event_btnAddAccActionPerformed

    private void btnExportFileChosseActionPerformed(final java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportFileChosseActionPerformed
        // TODO add your handling code here:
        final JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir") + "/");

        final FileNameExtensionFilter txtFilter = new FileNameExtensionFilter("Text files (*.txt)", "txt");

        fileChooser.setFileFilter(txtFilter);

        final int result = fileChooser.showSaveDialog(this);

        if (result == JFileChooser.APPROVE_OPTION) {

            exportFilePath = fileChooser.getSelectedFile().getAbsolutePath().concat(".txt");
            txtExportFilePath.setText(exportFilePath);
        }
    }//GEN-LAST:event_btnExportFileChosseActionPerformed

    private ArrayList<SubmitAddressInfo> generateSubmitAddressList(final int soluong) {

        final boolean isEditAddress = rdEditAddress.isSelected();

        final ArrayList<SubmitAddressInfo> submitAddressInfoList = new ArrayList<>();
        final ArrayList<String> phoneNumberList;
        final ArrayList<FullName> nameList;
        final ArrayList<Address> addressList;

        phoneNumberList = GeneratePhoneNumberService.generatePhoneNumber(soluong);
        nameList = GenerateNameService.generateName(soluong);
        LOG.debug("generateSubmitAddressList() :" + isEditAddress + " " + cbbPhuongXa.getSelectedIndex()
            + " " + cbbQuanHuyen.getSelectedIndex() + " " + cbbTinh.getSelectedIndex());
        if (!isEditAddress) {
            addressPath = "Sử dụng địa chỉ trong tài khoản";
            addressList = null;

        } else {

            if (cbbPhuongXa.getSelectedIndex() > 0) {
                LOG.debug("Gen theo Xa");
                addressPath = "Giao đến phường/xã " + cbbPhuongXa.getSelectedItem();
                addressList = GenerateAddressService.generateByTown(
                    townList.get(cbbPhuongXa.getSelectedIndex() - 1), soluong);

            } else if (cbbQuanHuyen.getSelectedIndex() > 0) {

                addressPath = "Giao đến Quận/Huyện " + cbbQuanHuyen.getSelectedItem();
                LOG.debug("Gen theo Quan");
                addressList = GenerateAddressService.generateByPrefecture(
                    prefectureList.get(cbbQuanHuyen.getSelectedIndex() - 1), soluong);

            } else if (cbbTinh.getSelectedIndex() > 0) {
                addressPath = "Giao đến Tỉnh/Thành phố " + cbbTinh.getSelectedItem();
                LOG.debug("Gen theo Tinh");
                addressList = GenerateAddressService.generateByCity(
                    cityList.get(cbbTinh.getSelectedIndex() - 1), soluong);

            } else {
                addressPath = "Giao đến đâu cũng được";
                LOG.debug("Gen ngau nhien ca nuoc");
                addressList = GenerateAddressService.generateNewAddress(soluong);
            }
        }

        for (int i = 0; i < soluong; i++) {
            submitAddressInfoList.add(new SubmitAddressInfo(addressList.get(i),
                nameList.get(i).getFullName(), phoneNumberList.get(i)));
        }
        return submitAddressInfoList;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddAcc;
    private javax.swing.JButton btnExportFileChosse;
    private javax.swing.JButton btnImportAcc;
    private javax.swing.JButton btnImportPhone;
    private javax.swing.JButton btnImportProxy;
    private javax.swing.JButton btnStartAutoBuy;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbbPhuongXa;
    private javax.swing.JComboBox<String> cbbQuanHuyen;
    private javax.swing.JComboBox<String> cbbTinh;
    private javax.swing.JCheckBox ckbMultiProductAllow;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblError;
    private javax.swing.JLabel lblLink;
    private javax.swing.JLabel lblPhoneBookName;
    private javax.swing.JLabel lblProxy;
    private javax.swing.JLabel lblSoLuong;
    private javax.swing.JLabel lblSoLuongAcc;
    private javax.swing.JLabel lblSoLuongError;
    private javax.swing.JLabel lblThreadError;
    private javax.swing.JLabel lblWait;
    private javax.swing.JProgressBar prbStatus;
    private javax.swing.JRadioButton rdAccountAddress;
    private javax.swing.JRadioButton rdEditAddress;
    private javax.swing.JTable tblImportedAccount;
    private javax.swing.JTable tblResult;
    private javax.swing.JTextField txtAccountFileName;
    private javax.swing.JTextField txtDelay;
    private javax.swing.JTextField txtExportFilePath;
    private javax.swing.JTextField txtNumOfOrder;
    private javax.swing.JTextField txtNumOfThread;
    private javax.swing.JTextArea txtPhoneNumber;
    private javax.swing.JTextField txtProductLink;
    private javax.swing.JTextArea txtProxy;
    private javax.swing.JTextField txtVoucher;
    // End of variables declaration//GEN-END:variables

    @Override
    public void callBack(final CheckoutTaskResult result) {
        LOG.debug("com.autocheckout.GUI.AutoBuy.callBack()");
        // STT - Success - Email - OrderID - Price
        final String[] reportStrings = {Integer.toString(index), result.isSuccess() ? "Thành Công" : "Thất bại", result.getEmail(),
            result.getOrderId(), result.getSubTotal()};

        model.addRow(reportStrings);

        if (result.isSuccess()) {
            success++;
        } else {
            fails++;
        }
        jLabel2.setText(String.format("%d thành công / %d thất bại", success, fails));

        prbStatus.setValue(tmpIndex);
        prbStatus.setStringPainted(true);
        index++;
        tmpIndex++;
    }

    @Override
    public void callBackWhenDone(final String message) {

        if (message != null) {
            JOptionPane.showConfirmDialog(this, message, "Ting Ting", JOptionPane.WARNING_MESSAGE);
        } else {
            JOptionPane.showConfirmDialog(this, "Đã đặt hàng xong", "Ting Ting", JOptionPane.WARNING_MESSAGE);
        }
        btnStartAutoBuy.setEnabled(true);
    }

    private void startAutoBuy(final int numOfThread, final String productUrl, final int numOfOrders,
        final String voucherCode, final ArrayList<SubmitAddressInfo> newAddressList) {
        final CheckoutTaskDispatcher checkoutTaskDispatcher
            = new CheckoutTaskDispatcher(this, exportFilePath, numOfThread, accountList, productUrl, numOfOrders, voucherCode, newAddressList, proxies
            );

        new Thread(new Runnable() {
            @Override

            public void run() {
                checkoutTaskDispatcher.process();
            }
        }).start();
    }

    private void showWait() {
        lblWait.setText(String.format("Waitting.. %d time", wattingTime++));
    }

}
