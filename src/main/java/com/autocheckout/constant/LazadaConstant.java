package com.autocheckout.constant;

public final class LazadaConstant {

    public static final String LOGIN_API_URL = "https://member.lazada.vn/user/api/login";

    public static final String LOGIN_PAGE_URL = "https://member.lazada.vn/user/login";

    public static final String VOUCHER_CHECKOUT_API_URL = "https://checkout.lazada.vn/checkout/api/async";
    public static final String CHECKOUT_API_URL = "https://checkout.lazada.vn/payment-cashier/api/async";

    public static final String RECENT_ORDER = "https://my.lazada.vn/api/recentOrders/";

    public static final String CUSTOMER_ORDER = "https://my.lazada.vn/customer/order/view/?tradeOrderId=%s";

    public static final String ADD_TO_CART_API_URL = "https://cart.lazada.vn/cart/api/add";

    //  public static final String SHIPPING_PAGE_URL = "https://checkout.lazada.vn/shipping";
    public static final String SHIPPING_PAGE_URL = "https://checkout.lazada.vn/shipping?spm=a2o4n.cart.proceed_to_checkout.proceed_to_checkout";

// public static final String PAYMENT_PAGE_URL = "https://checkout.lazada.vn/payment";
    public static final String PAYMENT_PAGE_URL = "https://checkout.lazada.vn/payment-cashier?checkoutOrderId=%s&entranceName=OM";

    public static final String REG_ACC_API_LINK = "https://member.lazada.vn/user/api/register";

    public static final String CHECK_EMAIL = "https://member.lazada.vn/user/api/checkEmailUsage?email=";

    public static final String CHECK_PHONE_NUMBER = "https://member.lazada.vn/user/api/validatePhone";

    public static final String GET_ADDRESS = "https://member.lazada.vn/locationtree/api/getSubAddressList?countryCode=";

    public static final String SUBMIT_ADDRESS = "https://member.lazada.vn/address/api/createAddress";

    public static final String USER_INFO_API_URL = "https://member.lazada.vn/user/api/getUser";

    public static final String AFFILIATE_URL = "https://www.lazada.vn/searchbox/?ajax=true&q=t-blade-blade&m=affiliate";

    public static final String CHECK_ORDER_URL = "https://my.lazada.vn/customer/order/view/?tradeOrderId=%s&buyerEmail=%s";

    public static final String GET_ADDRESS_INFO_URL = "https://member.lazada.vn/address/api/listAddress";

    public static final String UPDATE_ADDRESS_URL = "https://member.lazada.vn/address/api/updateAddress";

    public static final String CART_COUNT_ITEMS_URL = "https://cart.lazada.vn/cart/api/count";

    public static final String CART_PAGE_URL = "https://cart.lazada.vn/cart";

    public static final String CART_API_URL = "https://cart.lazada.vn/cart/api/async";

    public final class FILE_PATH {

        public static final String ORDER_TEMPORADY = "/OrderTemp.txt";

        public static final String REGISTED_ACC_WITHOUT_ADDRESS_PATH = "/Lazada/RegistedWithoutAddressAccountData.txt";

        public static final String REGISTED_ACC_PATH = "/Lazada/RegistedAccountData.txt";

        public static final String REGISTED_ACC_WITH_ADDRESS_PATH = "/Lazada/RegistedWithAddressAccountData.txt";

        public static final String PARENT_NAME_FILE_PATH = "/Lazada/ParentName.txt";

        public static final String NAME_FILE_PATH = "/Lazada/Name.txt";

        public static final String SUB_NAME_FILE_PATH = "/Lazada/SubName.txt";

        public static final String EMAIL_DOMAIN_PATH = "/Lazada/EmailDomain.txt";

        public static final String ADDRESS_DATA_PATH = "/Lazada/AddressData.txt";

        public static final String DETAILS_ADDRESS_DATA_PATH = "/Lazada/DetailAddress.txt";

        public static final String PHONE_BOOK_PATH = "/Lazada/PhoneNumber.txt";

        public static final String DEFAULT_ACCOUNT_PATH = "/Lazada/TempAccountTest.txt";

        public static final String DEFAULT_EXPORT_ORDER_PATH = "/Lazada/OrderList.txt";
    }

    public final class DEFAULT_VALUE {

        public static final String PASSWORD = "abcd1234";

        public static final String NAME = "Anh";

        public static final String NAME_EMAIL = "anh";

        public static final String SUB_NAME = "Công";

        public static final String SUB_NAME_EMAIL = "cong";

        public static final String FIRSTNAME = "Đào Trần";

        public static final String FIRST_NAME_EMAIL = "daotran";

        public static final String EMAIL_DOMAIN = "@gmail.com";

        public static final String GENDER = "male";

        public static final String CHECK_MAIL_OK = "NONE";

        public static final String CHECK_PHONE_OK = "NONE";
    }

    private LazadaConstant() {
    }
}
