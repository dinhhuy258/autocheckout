/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.constant;

/**
 *
 * @author anhdtc-jp
 */
public final class Constants {

    public final class DEFAULT {

        public static final int NUMBER_OF_THREAD = 100;

        public static final int DELAY_TIME = 60;
    }

    public final class SYMBOL {

        public static final String SPLIT_STRING_SYMBOL = "/";
    }

    public final class MAIL {

        public static final String USER_NAME = "autobuyitem@gmail.com";

        public static final String PASSWORD = "vitienlamtatca";
    }

    public final class FILE_PATH {

        public static final String RESOURCES_PATH = "/resources";

        public static final String PROXY_PATH = "/Proxies.txt";

        public static final String LOG_INFO_PATH = "/Information.log";

        public static final String LOG_ERROR_PATH = "/error.log";

        public static final String EXCEL_EXPORT_FOLDER = "/Excel Report";

        public static final String TXT_OUTPUT_FOLDER = "/Txt Output";

    }

    public final class IMAGE_RESOUCE {

        public static final String LOADING_GIF = "/Image/Loading.gif";
    }
}
