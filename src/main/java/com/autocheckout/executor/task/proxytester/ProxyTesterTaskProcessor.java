package com.autocheckout.executor.task.proxytester;

import java.util.concurrent.Callable;

import com.autocheckout.executor.flow.ProxyTesterFlow;

public class ProxyTesterTaskProcessor implements Callable<ProxyTesterTaskResult> {

    private final String proxy;

    public ProxyTesterTaskProcessor(final String proxy) {
        this.proxy = proxy;
    }

    @Override
    public ProxyTesterTaskResult call() throws Exception {
        final ProxyTesterFlow proxyTesterFlow = new ProxyTesterFlow(proxy);
        proxyTesterFlow.startFlow();
        return proxyTesterFlow.getFlowResult();
    }
}
