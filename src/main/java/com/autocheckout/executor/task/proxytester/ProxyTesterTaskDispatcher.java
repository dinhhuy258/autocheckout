package com.autocheckout.executor.task.proxytester;

import com.autocheckout.LOG;
import com.autocheckout.executor.task.AbstractTaskDispatcher;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

public class ProxyTesterTaskDispatcher extends AbstractTaskDispatcher {

    final List<String> proxies;

    public ProxyTesterTaskDispatcher(final int numThreads, final List<String> proxies) {
        super(numThreads, null);
        this.proxies = proxies;
    }

    @Override
    public void internalProcess() {
        final int numProxies = proxies.size();
        final List<Future<ProxyTesterTaskResult>> futures = new ArrayList<>();
        for (int i = 0; i < numProxies; ++i) {
            executorService.submit(new ProxyTesterTaskProcessor(proxies.get(i)));
        }
        LOG.debug("Submitting of threads finished... wait for processing of " + futures.size() + " futures");
        for (final Future<ProxyTesterTaskResult> future : futures) {
            try {
                final ProxyTesterTaskResult proxyTesterTaskResult = future.get();
                if (proxyTesterTaskResult.isSuccess()) {
                    LOG.debug("Success: " + proxyTesterTaskResult.getProxy());
                } else {
                    LOG.debug("Fail: " + proxyTesterTaskResult.getProxy());
                }
            } catch (final Exception e) {
                LOG.debug("There is problem while checking proxy: " + e.getMessage());
            }
        }
    }
}
