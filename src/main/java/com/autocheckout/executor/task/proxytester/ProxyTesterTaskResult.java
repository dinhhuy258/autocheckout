package com.autocheckout.executor.task.proxytester;

import com.autocheckout.executor.task.TaskResult;

public class ProxyTesterTaskResult extends TaskResult {

    private final String proxy;

    public static ProxyTesterTaskResult success(final String proxy) {
        return new ProxyTesterTaskResult(proxy,true, null);
    }

    public static ProxyTesterTaskResult fail(final String proxy) {
        return new ProxyTesterTaskResult(proxy,false, null);
    }

    private ProxyTesterTaskResult(final String proxy, final boolean success, final String message) {
        super(success, message);
        this.proxy = proxy;
    }

    public String getProxy() {
        return proxy;
    }
}
