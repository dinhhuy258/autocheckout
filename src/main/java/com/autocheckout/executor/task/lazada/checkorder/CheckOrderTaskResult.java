package com.autocheckout.executor.task.lazada.checkorder;

import com.autocheckout.data.lazada.orderdetail.OrderDetail;
import com.autocheckout.executor.task.TaskResult;

public class CheckOrderTaskResult extends TaskResult {

    private final OrderDetail orderDetail;

    public static CheckOrderTaskResult success(final OrderDetail orderDetail) {
        return new CheckOrderTaskResult(true, "Kiểm tra đơn hàng thành công", orderDetail);
    }

    public static CheckOrderTaskResult fail(final String message) {
        return new CheckOrderTaskResult(false, message, null);
    }

    private CheckOrderTaskResult(final boolean success, final String message, final OrderDetail orderDetail) {
        super(success, message);
        this.orderDetail = orderDetail;
    }

    public OrderDetail getOrderDetail() {
        return orderDetail;
    }
}
