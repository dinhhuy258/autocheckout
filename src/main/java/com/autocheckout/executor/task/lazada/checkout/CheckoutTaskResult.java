package com.autocheckout.executor.task.lazada.checkout;

import com.autocheckout.executor.task.TaskResult;

public class CheckoutTaskResult extends TaskResult {

    private final String email;

    private final String orderId;

    private final String subTotal;

    private final String shippingFee;

    private final String voucher;

    private final String totalAmount;

    public static CheckoutTaskResult fail(final String message) {
        return new CheckoutTaskResult(false, message, null, null, null, null, null, null);
    }

    public static CheckoutTaskResult success(final String email, final String orderId, final String subTotal,
                                             final String shippingFee, final String voucher, final String totalAmount) {
        return new CheckoutTaskResult(true, "Đặt hàng thành công", email, orderId, subTotal, shippingFee, voucher, totalAmount);
    }

    private CheckoutTaskResult(final boolean success, final String message,
                               final String email, final String orderId,
                               final String subTotal, final String shippingFee, final String voucher, final String totalAmount) {
        super(success, message);
        this.email = email;
        this.orderId = orderId;
        this.subTotal = subTotal;
        this.shippingFee = shippingFee;
        this.voucher = voucher;
        this.totalAmount = totalAmount;
    }

    public String getEmail() {
        return email;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public String getShippingFee() {
        return shippingFee;
    }

    public String getVoucher() {
        return voucher;
    }

    public String getTotalAmount() {
        return totalAmount;
    }
}
