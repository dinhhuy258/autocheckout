package com.autocheckout.executor.task.lazada.createaccount;

import com.autocheckout.executor.task.TaskResult;

public class CreateAccountTaskResult extends TaskResult {

    private final String email;

    private final String password;

    private final String address;

    public static CreateAccountTaskResult success(final String email, final String password, final String address) {
        return new CreateAccountTaskResult(true, "Tạo tài khoản thành công", email, password, address);
    }

    public static CreateAccountTaskResult fail(final String message) {
        return new CreateAccountTaskResult(false, message, null, null, null);
    }

    public CreateAccountTaskResult(final boolean success, final String message,
                                   final String email, final String password, final String address) {
        super(success, message);
        this.email = email;
        this.password = password;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

}
