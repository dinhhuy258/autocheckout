package com.autocheckout.executor.task.lazada.checkout;

import com.autocheckout.LOG;
import com.autocheckout.executor.flow.lazada.CheckoutFlow;
import java.util.concurrent.Callable;

public class CheckoutTaskProcessor implements Callable<CheckoutTaskResult> {

    private final CheckoutTaskSetting checkoutTaskSetting;

    public CheckoutTaskProcessor(final CheckoutTaskSetting checkoutTaskSetting) {
        this.checkoutTaskSetting = checkoutTaskSetting;
    }

    @Override
    public CheckoutTaskResult call() {
        final long startTime = System.currentTimeMillis();
        final CheckoutFlow checkoutFlow = new CheckoutFlow(checkoutTaskSetting.getEmail(), checkoutTaskSetting.getPassword(),
            checkoutTaskSetting.getProductUrl(), checkoutTaskSetting.getVoucherCode(), checkoutTaskSetting.getNewAddress(),
            checkoutTaskSetting.getProxy());
        checkoutFlow.startFlow();
        final long stopTime = System.currentTimeMillis();
        LOG.debug("Finish order: " + (stopTime - startTime));
        return checkoutFlow.getFlowResult();
    }
}
