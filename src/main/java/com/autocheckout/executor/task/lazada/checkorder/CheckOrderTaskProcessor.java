package com.autocheckout.executor.task.lazada.checkorder;

import java.util.concurrent.Callable;

import com.autocheckout.executor.flow.lazada.CheckOrderFlow;

public class CheckOrderTaskProcessor implements Callable<CheckOrderTaskResult> {

    private final CheckOrderTaskSetting checkOrderTaskSetting;

    public CheckOrderTaskProcessor(final CheckOrderTaskSetting checkOrderTaskSetting) {
        this.checkOrderTaskSetting = checkOrderTaskSetting;
    }

    @Override
    public CheckOrderTaskResult call() throws Exception {
        final CheckOrderFlow checkOrderFlow = new CheckOrderFlow(checkOrderTaskSetting.getProxy(), checkOrderTaskSetting.getBuyerEmail(), checkOrderTaskSetting.getOrderId());
        checkOrderFlow.startFlow();
        return checkOrderFlow.getFlowResult();
    }
}
