package com.autocheckout.executor.task.lazada.updateaddress;

import com.autocheckout.data.lazada.account.SubmitAddressInfo;

public class UpdateAddressTaskSetting {

    private final String email;

    private final String password;

    private final SubmitAddressInfo addressInfo;

    private final String proxy;

    public UpdateAddressTaskSetting(final String email, final String password, final SubmitAddressInfo addressInfo, final String proxy) {
        this.email = email;
        this.password = password;
        this.addressInfo = addressInfo;
        this.proxy = proxy;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public SubmitAddressInfo getAddressInfo() {
        return addressInfo;
    }

    public String getProxy() {
        return proxy;
    }
}
