/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.executor.task.lazada.createaccount;

import java.util.concurrent.Callable;

import com.autocheckout.data.lazada.account.RegistrationAccountInfo;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.flow.lazada.CreateAccountFlow;

/**
 *
 * @author anhdtc-jp
 */
public class CreateAccountTaskProcessor implements Callable<CreateAccountTaskResult> {

    private final RegistrationAccountInfo account;

    private final SubmitAddressInfo addressInfo;

    private final String proxy;

    public CreateAccountTaskProcessor(final String proxy, final RegistrationAccountInfo account, final SubmitAddressInfo addressInfo) {
        this.proxy = proxy;
        this.account = account;
        this.addressInfo = addressInfo;
    }

    @Override
    public CreateAccountTaskResult call() throws Exception {
        final CreateAccountFlow createAccountFlow = new CreateAccountFlow(proxy, account, addressInfo);
        createAccountFlow.startFlow();
        return createAccountFlow.getFlowResult();
    }

}
