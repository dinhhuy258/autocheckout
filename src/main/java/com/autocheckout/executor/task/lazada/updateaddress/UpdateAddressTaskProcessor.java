package com.autocheckout.executor.task.lazada.updateaddress;

import java.util.concurrent.Callable;

import com.autocheckout.executor.flow.lazada.UpdateAddressFlow;

public class UpdateAddressTaskProcessor implements Callable<UpdateAddressTaskResult> {

    private final UpdateAddressTaskSetting updateAddressTaskSetting;

    public UpdateAddressTaskProcessor(final UpdateAddressTaskSetting updateAddressTaskSetting) {
        this.updateAddressTaskSetting = updateAddressTaskSetting;
    }

    @Override
    public UpdateAddressTaskResult call() throws Exception {
        final UpdateAddressFlow updateAddressFlow = new UpdateAddressFlow(updateAddressTaskSetting.getEmail(), updateAddressTaskSetting.getPassword(),
                updateAddressTaskSetting.getAddressInfo(), updateAddressTaskSetting.getProxy());
        updateAddressFlow.startFlow();
        return updateAddressFlow.getFlowResult();
    }

}
