package com.autocheckout.executor.task.lazada.checkorder;

import com.autocheckout.LOG;
import com.autocheckout.callback.ICallback;
import com.autocheckout.data.lazada.orderdetail.OrderDetail;
import com.autocheckout.executor.task.AbstractTaskDispatcher;
import com.autocheckout.util.CircularList;
import com.autocheckout.util.ExcelWriterUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class CheckOrderTaskDispatcher extends AbstractTaskDispatcher {

    private final String outputFile;

    private final CircularList<String> proxies;

    private final List<OrderDetail> orders;

    private boolean cancel;

    public CheckOrderTaskDispatcher(final ICallback target, final int numThreads, final List<OrderDetail> orders,
        final String outputFolder, final CircularList<String> proxies) {
        super(numThreads, target);
        outputFile = outputFolder;
        this.proxies = proxies;
        this.orders = orders;
        cancel = false;
    }

    public void cancel() {
        cancel = true;
    }

    @Override
    public void internalProcess() {
        cancel = false;
        final int numOfOrders = orders.size();
        final List<Future<CheckOrderTaskResult>> futures = new ArrayList<>();

        if (proxies == null || proxies.isEmpty()) {
            // Do not use proxy
            for (int i = 0; i < numOfOrders; ++i) {
                futures.add(executorService.submit(new CheckOrderTaskProcessor(new CheckOrderTaskSetting(orders.get(i), null))));
            }
        } else {
            // Using proxy
            for (int i = 0; i < numOfOrders; ++i) {
                futures.add(executorService.submit(new CheckOrderTaskProcessor(new CheckOrderTaskSetting(orders.get(i), proxies.get(i)))));
            }
        }

        LOG.debug("Submitting of threads finished... wait for processing of " + futures.size() + " futures");
        final List<OrderDetail> orderDetails = new ArrayList<>();
        for (final Future<CheckOrderTaskResult> future : futures) {
            try {
                if (cancel) {
                    break;
                }
                final CheckOrderTaskResult checkOrderTaskResult = future.get();
                if (checkOrderTaskResult.isSuccess()) {
                    target.callBack(checkOrderTaskResult);
                    orderDetails.add(checkOrderTaskResult.getOrderDetail());
                } else {
                    target.callBack(checkOrderTaskResult);
                }
            } catch (final InterruptedException | ExecutionException e) {
                target.callBack(CheckOrderTaskResult.fail("Lỗi đường truyền"));
            }
        }

        if (!orderDetails.isEmpty()) {
            // Done
            try {
                ExcelWriterUtils.exportOrderDetailListToExcel(orderDetails, outputFile);
                target.callBackWhenDone("Đã xuất file thành công \n " + outputFile);
            } catch (final IOException e) {
                // There is problem while exporting to excel file
                LOG.debug("There is problem while exporting order details to excel file");
                target.callBackWhenDone("Có lỗi khi xuất báo cáo  \n" + outputFile);
            }
        } else {
            LOG.debug("Can not processing any order information.");
            target.callBackWhenDone("Không có đơn hàng nào để xuất file cả.");
        }
    }
}
