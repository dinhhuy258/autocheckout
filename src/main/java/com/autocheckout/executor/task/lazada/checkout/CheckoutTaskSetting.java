package com.autocheckout.executor.task.lazada.checkout;

import com.autocheckout.data.lazada.account.SubmitAddressInfo;

public class CheckoutTaskSetting {

    private final String email;

    private final String password;

    private final String productUrl;

    private final String voucherCode;

    private final SubmitAddressInfo newAddress;

    private final String proxy;

    public CheckoutTaskSetting(final String email, final String password, final String productUrl, final String voucherCode, final SubmitAddressInfo newAddress, final String proxy) {
        this.email = email;
        this.password = password;
        this.productUrl = productUrl;
        this.proxy = proxy;
        this.voucherCode = voucherCode;
        this.newAddress = newAddress;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public String getProxy() {
        return proxy;
    }

    public SubmitAddressInfo getNewAddress() {
        return newAddress;
    }
}
