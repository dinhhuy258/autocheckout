package com.autocheckout.executor.task.lazada.updateaddress;

import com.autocheckout.executor.task.TaskResult;

public class UpdateAddressTaskResult extends TaskResult {

    public static UpdateAddressTaskResult fail(final String message) {
        return new UpdateAddressTaskResult(false, message);
    }

    public static UpdateAddressTaskResult success(final String message) {
        return new UpdateAddressTaskResult(true, message);
    }

    public UpdateAddressTaskResult(final boolean success, final String message) {
        super(success, message);
    }
}
