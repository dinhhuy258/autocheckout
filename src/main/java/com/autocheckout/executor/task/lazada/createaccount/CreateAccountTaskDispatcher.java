/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.executor.task.lazada.createaccount;

import com.autocheckout.LOG;
import com.autocheckout.callback.ICallback;
import com.autocheckout.constant.Constants;
import com.autocheckout.data.lazada.account.RegistrationAccountInfo;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.task.AbstractTaskDispatcher;
import com.autocheckout.util.CircularList;
import com.autocheckout.util.FileUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 *
 * @author anhdtc-jp
 */
public class CreateAccountTaskDispatcher extends AbstractTaskDispatcher {

    private final ArrayList<RegistrationAccountInfo> accounts;

    private final ArrayList<SubmitAddressInfo> addressInfo;

    private final CircularList<String> proxies;

    private final String exportFilePath;

    private boolean cancel;

    public CreateAccountTaskDispatcher(final ICallback target, final String exportFilePath, final int numThreads, final CircularList<String> proxies,
        final ArrayList<RegistrationAccountInfo> accounts, final ArrayList<SubmitAddressInfo> addressInfo) {
        super(numThreads, target);
        this.accounts = accounts;
        this.addressInfo = addressInfo;
        this.proxies = proxies;
        this.exportFilePath = exportFilePath;
        cancel = false;
    }

    @Override
    public void internalProcess() {
        cancel = false;
        final int numberAccounts = accounts.size();
        final List<Future<CreateAccountTaskResult>> futures = new ArrayList<>();
        if (proxies == null || proxies.isEmpty()) {
            // Do not use proxy
            for (int i = 0; i < numberAccounts; ++i) {
                futures.add(executorService.submit(new CreateAccountTaskProcessor(null, accounts.get(i), addressInfo.get(i))));
            }
        } else {
            // Using proxy
            for (int i = 0; i < numberAccounts; ++i) {
                futures.add(executorService.submit(new CreateAccountTaskProcessor(proxies.get(i), accounts.get(i), addressInfo.get(i))));
            }
        }
        LOG.debug("Submitting of threads finished... wait for processing of " + futures.size() + " futures");
        for (final Future<CreateAccountTaskResult> future : futures) {
            try {
                if (cancel) {
                    break;
                }

                final CreateAccountTaskResult createAccountTaskResult = future.get();
                if (createAccountTaskResult != null && createAccountTaskResult.isSuccess()) {

                    FileUtils.exportToFileFullPath(exportFilePath,
                        String.join(Constants.SYMBOL.SPLIT_STRING_SYMBOL, createAccountTaskResult.getEmail(),
                            createAccountTaskResult.getPassword()));

                    target.callBack(createAccountTaskResult);
                } else {
                    // In the case we have an error
                }
            } catch (final InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        // Show done message
        target.callBackWhenDone(null);
    }

    public void cancel() {
        cancel = true;
    }

}
