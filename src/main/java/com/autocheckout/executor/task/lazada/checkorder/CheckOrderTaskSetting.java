package com.autocheckout.executor.task.lazada.checkorder;

import com.autocheckout.data.lazada.orderdetail.OrderDetail;

public class CheckOrderTaskSetting {

    private final String buyerEmail;

    private final String orderId;

    private final String proxy;

    public CheckOrderTaskSetting(final OrderDetail orderDetails, final String proxy) {
        buyerEmail = orderDetails.getBuyerEmail();
        orderId = orderDetails.getOrderId();
        this.proxy = proxy;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getProxy() {
        return proxy;
    }
}
