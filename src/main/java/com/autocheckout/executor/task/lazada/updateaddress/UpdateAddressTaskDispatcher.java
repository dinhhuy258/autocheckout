package com.autocheckout.executor.task.lazada.updateaddress;

import com.autocheckout.LOG;
import com.autocheckout.data.lazada.account.Account;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.task.AbstractTaskDispatcher;
import com.autocheckout.util.CircularList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

public class UpdateAddressTaskDispatcher extends AbstractTaskDispatcher {

    private final List<Account> accounts;

    private final List<SubmitAddressInfo> adressInfo;

    private final CircularList<String> proxies;

    public UpdateAddressTaskDispatcher(final int numThreads, final List<Account> accounts,
        final List<SubmitAddressInfo> adressInfo,
        final CircularList<String> proxies) {
        super(numThreads, null);
        this.accounts = accounts;
        this.adressInfo = adressInfo;
        this.proxies = proxies;
    }

    @Override
    public void internalProcess() {
        final List<Future<UpdateAddressTaskResult>> futures = new ArrayList<>();
        final int numOfAccount = accounts.size();
        if (proxies == null || proxies.isEmpty()) {
            LOG.debug("InternalProcess(): Without proxy");
            // Do not use proxy
            for (int i = 0; i < numOfAccount; ++i) {
                futures.add(executorService.submit(
                    new UpdateAddressTaskProcessor(createUpdateAddressTaskSetting(accounts.get(i).getEmail(),
                        accounts.get(i).getPassword(), adressInfo.get(i), null))));
            }
        } else {
            LOG.debug("InternalProcess(): Using proxy");
            // Using proxy
            for (int i = 0; i < numOfAccount; ++i) {
                futures.add(executorService.submit(
                    new UpdateAddressTaskProcessor(createUpdateAddressTaskSetting(accounts.get(i).getEmail(),
                        accounts.get(i).getPassword(), adressInfo.get(i), proxies.get(i)))));
            }
        }
        LOG.debug("Submitting of threads finished... wait for processing of " + futures.size() + " futures");

        for (final Future<UpdateAddressTaskResult> future : futures) {
            try {
                final UpdateAddressTaskResult updateAddressTaskResult = future.get();
                if (updateAddressTaskResult != null && updateAddressTaskResult.isSuccess()) {
                    target.callBack(UpdateAddressTaskResult.success("Cập nhật địa chỉ thành công."));
                } else {
                    target.callBack(UpdateAddressTaskResult.fail("Cập nhật địa chỉ thất bại."));
                }
            } catch (final Exception e) {
                target.callBack(UpdateAddressTaskResult.fail("Xảy ra lỗi trong quá trình cập nhật địa chỉ."));
                e.printStackTrace();
            }
        }
        // Show done message
        target.callBackWhenDone(null);
    }

    private UpdateAddressTaskSetting createUpdateAddressTaskSetting(final String email, final String password, final SubmitAddressInfo addressInfo, final String proxy) {
        final UpdateAddressTaskSetting updateAddressTaskSetting = new UpdateAddressTaskSetting(email, password, addressInfo, proxy);
        return updateAddressTaskSetting;
    }
}
