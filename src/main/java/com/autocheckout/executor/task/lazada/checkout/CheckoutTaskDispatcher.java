package com.autocheckout.executor.task.lazada.checkout;

import com.autocheckout.LOG;
import com.autocheckout.callback.ICallback;
import com.autocheckout.constant.Constants;
import com.autocheckout.data.lazada.account.Account;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.task.AbstractTaskDispatcher;
import com.autocheckout.util.CircularList;
import com.autocheckout.util.DateUtils;
import com.autocheckout.util.EmailUtils;
import com.autocheckout.util.FileUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import org.apache.commons.collections4.CollectionUtils;

public class CheckoutTaskDispatcher extends AbstractTaskDispatcher {

    private final List<Account> accounts;

    private final List<SubmitAddressInfo> newAdress;

    private final CircularList<String> proxies;

    private final String productUrl;

    private final String voucherCode;

    private final String exportFilePath;

    private final int numOfOrder;

    private boolean cancel;

    public CheckoutTaskDispatcher(final ICallback target, final String exportFilePath, final int numThreads, final CircularList<Account> accounts,
        final String productUrl, final int numOfOrder, final String voucherCode, final List<SubmitAddressInfo> newAdress,
        final CircularList<String> proxies) {
        super(numThreads, target);
        this.accounts = accounts;
        this.productUrl = productUrl;
        this.proxies = proxies;
        this.voucherCode = voucherCode;
        this.newAdress = newAdress;
        this.exportFilePath = exportFilePath;
        this.numOfOrder = numOfOrder;
        cancel = false;
    }

    @Override
    public void internalProcess() {
        cancel = false;
        final List<Future<CheckoutTaskResult>> futures = new ArrayList<>();
        if (proxies == null || proxies.isEmpty()) {
            // Do not use proxy
            for (int i = 0; i < numOfOrder; ++i) {
                if (CollectionUtils.isNotEmpty(newAdress)) {
                    futures.add(executorService.submit(new CheckoutTaskProcessor(createCheckoutTaskSetting(accounts.get(i),
                        newAdress.get(i), null))));
                } else {
                    futures.add(executorService.submit(new CheckoutTaskProcessor(createCheckoutTaskSetting(accounts.get(i),
                        null, null))));
                }
            }
        } else {
            // Using proxy
            for (int i = 0; i < numOfOrder; ++i) {
                if (CollectionUtils.isNotEmpty(newAdress)) {
                    futures.add(executorService.submit(
                        new CheckoutTaskProcessor(createCheckoutTaskSetting(accounts.get(i), newAdress.get(i), proxies.get(i)))));
                } else {
                    futures.add(executorService.submit(
                        new CheckoutTaskProcessor(createCheckoutTaskSetting(accounts.get(i), null, proxies.get(i)))));
                }
            }
        }
        LOG.debug("Submitting of threads finished... wait for processing of " + futures.size() + " futures");

        boolean hasAtLeastSuccess = false;
        for (final Future<CheckoutTaskResult> future : futures) {
            try {
                if (cancel) {
                    break;
                }

                final CheckoutTaskResult checkoutTaskResult = future.get();

                if (checkoutTaskResult.isSuccess()) {
                    hasAtLeastSuccess = true;
                    LOG.debug("Order sussess: " + String.join(" ", checkoutTaskResult.getEmail(),
                        checkoutTaskResult.getOrderId(), checkoutTaskResult.getSubTotal(),
                        checkoutTaskResult.getShippingFee(), checkoutTaskResult.getVoucher(),
                        checkoutTaskResult.getTotalAmount()));

                    FileUtils.exportToFileFullPath(exportFilePath,
                        String.join(Constants.SYMBOL.SPLIT_STRING_SYMBOL, checkoutTaskResult.getEmail(),
                            checkoutTaskResult.getOrderId()));

                    target.callBack(checkoutTaskResult);
                } else {
                    // In the case we have an error
                    LOG.debug("There is problem while order product");
                    target.callBack(checkoutTaskResult);
                }
            } catch (final Exception e) {
                LOG.debug("There is problem while order product: " + e.getMessage());
                target.callBack(CheckoutTaskResult.fail("Xảy ra lỗi trong quá trình mua hàng."));
            }
        }

        // Show done message
        target.callBackWhenDone(null);
        // Send mail
        if (hasAtLeastSuccess) {
            EmailUtils.sendMail(DateUtils.getDateTime() + "Checkout orders", "Checkout orders at:" + DateUtils.getDateTime(),
                exportFilePath);
        }
    }

    public void cancel() {
        cancel = true;
    }

    private CheckoutTaskSetting createCheckoutTaskSetting(final Account account, final SubmitAddressInfo newAdress,
        final String proxy) {
        return new CheckoutTaskSetting(account.getEmail(), account.getPassword(),
            productUrl, voucherCode, newAdress, proxy);
    }

}
