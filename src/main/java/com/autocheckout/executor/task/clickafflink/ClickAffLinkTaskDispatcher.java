package com.autocheckout.executor.task.clickafflink;

import com.autocheckout.LOG;
import com.autocheckout.executor.task.AbstractTaskDispatcher;
import com.autocheckout.util.CircularList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

public class ClickAffLinkTaskDispatcher extends AbstractTaskDispatcher {

    private final String affLink;

    private final int numOfClicks;

    private final CircularList<String> proxies;

    public ClickAffLinkTaskDispatcher(final int numThreads, final String affLink,
        final int numOfClicks, final CircularList<String> proxies) {
        super(numThreads, null);
        this.affLink = affLink;
        this.numOfClicks = numOfClicks;
        this.proxies = proxies;
    }

    @Override
    public void internalProcess() {
        final List<Future<ClickAffLinkTaskResult>> futures = new ArrayList<>();
        for (int i = 0; i < numOfClicks; ++i) {
            futures.add(executorService.submit(new ClickAffLinkTaskProcessor(affLink, proxies.get(i))));
        }
        LOG.debug("Submitting of threads finished... wait for processing of " + futures.size() + " futures");
        for (final Future<ClickAffLinkTaskResult> future : futures) {
            try {
                final ClickAffLinkTaskResult clickAffLinkTaskResult = future.get();
                if (clickAffLinkTaskResult.isSuccess()) {
                    LOG.debug("Click aff link success: " + affLink);
                } else {
                    LOG.debug("Click aff link fail: " + affLink);
                    LOG.debug(clickAffLinkTaskResult.getMessage());
                }
            } catch (final Exception e) {
                LOG.debug("There is problem while click aff link: " + e.getMessage());
            }
        }
    }
}
