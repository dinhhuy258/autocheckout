package com.autocheckout.executor.task.clickafflink;

import com.autocheckout.executor.task.TaskResult;

public class ClickAffLinkTaskResult extends TaskResult {

    public static ClickAffLinkTaskResult success() {
        return new ClickAffLinkTaskResult(true, null);
    }

    public static ClickAffLinkTaskResult fail(final String message) {
        return new ClickAffLinkTaskResult(false, message);
    }

    public ClickAffLinkTaskResult(final boolean success, final String message) {
        super(success, message);
    }
}
