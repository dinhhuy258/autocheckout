package com.autocheckout.executor.task.clickafflink;

import java.util.concurrent.Callable;

import com.autocheckout.executor.flow.ClickAffLinkFlow;

public class ClickAffLinkTaskProcessor implements Callable<ClickAffLinkTaskResult> {

    private final String affLink;

    private final String proxy;

    public ClickAffLinkTaskProcessor(final String affLink, final String proxy) {
        this.affLink = affLink;
        this.proxy = proxy;
    }

    @Override
    public ClickAffLinkTaskResult call() throws Exception {
        final ClickAffLinkFlow clickAffLinkFlow = new ClickAffLinkFlow(affLink, proxy);
        clickAffLinkFlow.startFlow();
        return clickAffLinkFlow.getFlowResult();
    }
}
