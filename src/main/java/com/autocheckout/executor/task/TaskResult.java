package com.autocheckout.executor.task;

public abstract class TaskResult {

    protected final boolean success;

    protected final String message;

    public TaskResult(final boolean success, final String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
