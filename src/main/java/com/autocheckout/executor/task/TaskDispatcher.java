package com.autocheckout.executor.task;

public interface TaskDispatcher {
    
    void process();
}
