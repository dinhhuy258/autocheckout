package com.autocheckout.executor.task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.autocheckout.callback.ICallback;

public abstract class AbstractTaskDispatcher implements TaskDispatcher {

    protected final ExecutorService executorService;

    protected ICallback target;

    public AbstractTaskDispatcher(final int numThreads, final ICallback target) {
        this.target = target;
        executorService = Executors.newFixedThreadPool(numThreads);
    }

    @Override
    public final void process() {
        internalProcess();
        executorService.shutdownNow();
    }

    public abstract void internalProcess();
}
