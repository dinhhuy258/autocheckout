package com.autocheckout.executor.flow.lazada;

import com.autocheckout.data.lazada.account.Account;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.action.lazada.AddToCartAction;
import com.autocheckout.executor.action.lazada.AddVoucherCodeAction;
import com.autocheckout.executor.action.lazada.CheckCartAction;
import com.autocheckout.executor.action.lazada.CheckoutAction;
import com.autocheckout.executor.action.lazada.LoginAction;
import com.autocheckout.executor.action.lazada.UpdateAddressAction;
import com.autocheckout.executor.task.lazada.checkout.CheckoutTaskResult;

public class CheckoutFlow extends LazadaAbstractFlow<CheckoutTaskResult> {

    private final String email;

    public CheckoutFlow(final String email, final String password, final String productUrl,
        final String voucherCode, final SubmitAddressInfo newAddress, final String proxy) {
        super(proxy);

        this.email = email;
        actions.add(new LoginAction(this, new Account(email, password)));
        actions.add(new CheckCartAction(this));
        if (newAddress != null) {
            actions.add(new UpdateAddressAction(this, newAddress));
        }
        actions.add(new AddToCartAction(this, productUrl));
        if (voucherCode != null && !voucherCode.isEmpty()) {
            actions.add(new AddVoucherCodeAction(this, voucherCode));
        }
//        OrderTotalAction orderTotalAction = new OrderTotalAction(this);
//        actions.add(orderTotalAction);
        actions.add(new CheckoutAction(this));
    }

    public String getEmail() {
        return email;
    }

    @Override
    protected void flowFail(final String message) {
        flowResult = CheckoutTaskResult.fail(message);
    }

    @Override
    protected void flowSuccess(final Object data) {
        if (data != null && data instanceof CheckoutTaskResult) {
            flowResult = (CheckoutTaskResult) data;
        } else {
            System.out.printf("Expected CheckoutTaskResult object but got: " + data == null ? "null" : data.getClass().getCanonicalName());
            flowFail("Lỗi kỹ thuật, vui lòng báo cáo với người tạo ra ứng dụng này để kịp thời sửa chữa.");
        }
    }
}
