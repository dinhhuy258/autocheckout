package com.autocheckout.executor.flow.lazada;

import com.autocheckout.data.lazada.account.RegistrationAccountInfo;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.action.lazada.CreateAccountAction;
import com.autocheckout.executor.action.lazada.SetAddressAction;
import com.autocheckout.executor.task.lazada.createaccount.CreateAccountTaskResult;

public class CreateAccountFlow extends LazadaAbstractFlow<CreateAccountTaskResult> {

    private final String email;

    private final String password;

    public CreateAccountFlow(final String proxy, final RegistrationAccountInfo accountInfo, final SubmitAddressInfo addressInfo) {
        super(proxy);
        actions.add(new CreateAccountAction(this, accountInfo));
        actions.add(new SetAddressAction(this, addressInfo));
        email = accountInfo.getEmail();
        password = accountInfo.getPassword();
    }

    @Override
    protected void flowFail(final String message) {
        flowResult = CreateAccountTaskResult.fail(message) ;
    }

    @Override
    protected void flowSuccess(final Object data) {
        if (data != null && data instanceof CreateAccountTaskResult) {
            flowResult = (CreateAccountTaskResult) data;
        } else {
            System.out.printf("Expected CreateAccountTaskResult object but got: " + data == null ? "null" : data.getClass().getCanonicalName());
            flowFail("Lỗi kỹ thuật, vui lòng báo cáo với người tạo ra ứng dụng này để kịp thời sửa chữa.");
        }
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
