package com.autocheckout.executor.flow.lazada;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.exception.BotException;
import com.autocheckout.exception.TechniqueException;
import com.autocheckout.executor.flow.AbstractFlow;
import com.autocheckout.executor.task.TaskResult;

public abstract class LazadaAbstractFlow<T extends TaskResult> extends AbstractFlow<T> {

    private String csrfToken;

    public LazadaAbstractFlow(final String proxy) {
        super(proxy);
    }

    @Override
    protected void doBeforeFlow() throws BotException {
        try {
            final Document document = Jsoup.parse(httpConnector.doGet(LazadaConstant.LOGIN_PAGE_URL));
            csrfToken = document.select("head meta[name=X-CSRF-TOKEN]").first().attr("content");
        } catch (final Exception e) {
            if (e instanceof BotException) {
                throw e;
            }
            throw new TechniqueException("There is problem while getting csrf token", e);
        }
    }

    public String getCsrfToken() {
        return csrfToken;
    }
}
