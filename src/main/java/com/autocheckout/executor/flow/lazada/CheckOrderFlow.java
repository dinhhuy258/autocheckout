package com.autocheckout.executor.flow.lazada;

import com.autocheckout.executor.action.lazada.CheckOrderAction;
import com.autocheckout.executor.task.lazada.checkorder.CheckOrderTaskResult;

public class CheckOrderFlow extends LazadaAbstractFlow<CheckOrderTaskResult> {
    public CheckOrderFlow(final String proxy, final String buyerEmail, final String orderId) {
        super(proxy);
        actions.add(new CheckOrderAction(this, buyerEmail, orderId));
    }

    @Override
    protected void flowFail(final String message) {
        flowResult = CheckOrderTaskResult.fail(message);
    }

    @Override
    protected void flowSuccess(final Object data) {
        if (data != null && data instanceof CheckOrderTaskResult) {
            flowResult = (CheckOrderTaskResult) data;
        } else {
            System.out.printf("Expected CheckOrderResult object but got: " + data == null ? "null" : data.getClass().getCanonicalName());
            flowFail("Lỗi kỹ thuật, vui lòng báo cáo với người tạo ra ứng dụng này để kịp thời sửa chữa.");
        }
    }

    @Override
    protected void doAfterFlow() {
        super.doAfterFlow();
    }
}
