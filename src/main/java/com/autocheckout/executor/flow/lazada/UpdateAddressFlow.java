package com.autocheckout.executor.flow.lazada;

import com.autocheckout.data.lazada.account.Account;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.executor.action.lazada.LoginAction;
import com.autocheckout.executor.action.lazada.UpdateAddressAction;
import com.autocheckout.executor.task.lazada.updateaddress.UpdateAddressTaskResult;

public class UpdateAddressFlow extends LazadaAbstractFlow<UpdateAddressTaskResult> {

    public UpdateAddressFlow(final String email, final String password,
                             final SubmitAddressInfo addressInfo, final String proxy) {
        super(proxy);
        actions.add(new LoginAction(this, new Account(email, password)));
        actions.add(new UpdateAddressAction(this, addressInfo));
    }

    @Override
    protected void flowFail(final String message) {
        flowResult = UpdateAddressTaskResult.fail(message);
    }

    @Override
    protected void flowSuccess(final Object data) {
        if (data == null) {
            flowResult = UpdateAddressTaskResult.success("Cập nhật địa chỉ thành công");
        } if (data != null && data instanceof UpdateAddressTaskResult) {
            flowResult = (UpdateAddressTaskResult) data;
        } else {
            System.out.printf("Expected UpdateAddressTaskResult object but got: " + data == null ? "null" : data.getClass().getCanonicalName());
            flowFail("Lỗi kỹ thuật, vui lòng báo cáo với người tạo ra ứng dụng này để kịp thời sửa chữa.");
        }
    }
}
