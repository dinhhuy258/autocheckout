package com.autocheckout.executor.flow;

import com.autocheckout.LOG;
import com.autocheckout.exception.AffLinkNotSupportedException;
import com.autocheckout.exception.BotException;
import com.autocheckout.exception.BussinessException;
import com.autocheckout.exception.NetworkException;
import com.autocheckout.exception.ProxyException;
import com.autocheckout.exception.TechniqueException;
import com.autocheckout.executor.action.Action;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.task.TaskResult;
import com.autocheckout.http.HttpConnector;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFlow<T extends TaskResult> implements Flow {

    protected final HttpConnector httpConnector;

    protected final List<Action> actions;

    protected T flowResult;

    public AbstractFlow(final String proxy) {
        httpConnector = new HttpConnector(proxy);
        actions = new ArrayList<>();
    }

    @Override
    public void startFlow() {
        try {
            doBeforeFlow();
            if (actions.isEmpty()) {
                flowSuccess(null);
            }
            for (final Action action : actions) {
                final ActionResult actionResult = action.perform();
                if (!actionResult.isSuccess()) {
                    flowFail(actionResult.getMessage());
                    break;
                } else {
                    // If success
                    if (isTheLastAction(action)) {
                        // If it's it the last action of the flow
                        flowSuccess(actionResult.getData());
                    }
                }
            }
        } catch (final AffLinkNotSupportedException exception) {
            LOG.debug("Aff link is not supported");
            flowFail("Aff link không hỗ trợ.");
        } catch (final BussinessException exception) {
            // There is problem while configuring the data. Ex: product's url
            LOG.debug("There is problem while configuring the data. Ex: product's url");
            flowFail("Có lỗi xảy ra trong quá trình đặt hàng. Vui lòng kiểm tra lại link sản phẩm.");
        } catch (final ProxyException exception) {
            LOG.debug("Proxy is invalid");
            flowFail("Không thể kết nối đến proxy này");
        } catch (final NetworkException exception) {
            // There is problem with network or connect with proxy failed
            LOG.debug("Connect to server failed");
            flowFail("Mạng có vấn đề");
        } catch (final TechniqueException exception) {
            // Technique problem
            LOG.debug("Technique exception: " + exception.getMessage());
            flowFail("Lỗi kỹ thuật, vui lòng báo cáo với người tạo ra ứng dụng này để kịp thời sửa chữa.");
        } catch (final Exception exception) {
            System.out.printf("Error: " + exception.getMessage());
            flowFail("Lỗi kỹ thuật, vui lòng báo cáo với người tạo ra ứng dụng này để kịp thời sửa chữa.");
        } finally {
            doAfterFlow();
        }
    }

    protected abstract void flowFail(final String message);

    protected abstract void flowSuccess(final Object data);

    public HttpConnector getHttpConnector() {
        return httpConnector;
    }

    public T getFlowResult() {
        return flowResult;
    }

    protected void doBeforeFlow() throws BotException {
    }

    protected void doAfterFlow() {
        httpConnector.clearCookies();
    }

    private boolean isTheLastAction(final Action action) {
        return actions.get(actions.size() - 1).equals(action);
    }
}
