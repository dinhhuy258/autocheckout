package com.autocheckout.executor.flow;

import com.autocheckout.executor.action.ProxyTesterAction;
import com.autocheckout.executor.task.proxytester.ProxyTesterTaskResult;

public class ProxyTesterFlow extends AbstractFlow<ProxyTesterTaskResult> {

    private final String proxy;

    public ProxyTesterFlow(final String proxy) {
        super(proxy);
        this.proxy = proxy;
        actions.add(new ProxyTesterAction(this));
    }

    @Override
    protected void flowFail(final String message) {
        flowResult = ProxyTesterTaskResult.fail(proxy);
    }

    @Override
    protected void flowSuccess(final Object data) {
        flowResult = ProxyTesterTaskResult.success(proxy);
    }
}
