package com.autocheckout.executor.flow;

import com.autocheckout.executor.action.ClickAffLinkAction;
import com.autocheckout.executor.task.clickafflink.ClickAffLinkTaskResult;

public class ClickAffLinkFlow extends AbstractFlow<ClickAffLinkTaskResult> {

    public ClickAffLinkFlow(final String affLink, final String proxy) {
        super(proxy);
        actions.add(new ClickAffLinkAction(this, affLink));
    }

    @Override
    protected void flowFail(final String message) {
        flowResult = ClickAffLinkTaskResult.fail(message);
    }

    @Override
    protected void flowSuccess(final Object data) {
        flowResult = ClickAffLinkTaskResult.success();
    }
}
