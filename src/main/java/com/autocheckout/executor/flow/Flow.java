package com.autocheckout.executor.flow;

import com.autocheckout.exception.BotException;

public interface Flow {

    void startFlow() throws BotException;
}
