package com.autocheckout.executor.action;

import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.flow.AbstractFlow;

public class ProxyTesterAction extends AbstractAction {

    public ProxyTesterAction(final AbstractFlow flow) {
        super(flow);
    }

    @Override
    public ActionResult perform() throws BotException {
        flow.getHttpConnector().doGet(LazadaConstant.LOGIN_API_URL);
        return ActionResult.success();
    }
}
