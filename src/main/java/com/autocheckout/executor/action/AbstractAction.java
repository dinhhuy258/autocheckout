package com.autocheckout.executor.action;

import com.autocheckout.executor.flow.AbstractFlow;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class AbstractAction<T extends AbstractFlow> implements Action {

    protected final Gson gson;

    protected final T flow;

    public AbstractAction(final T flow) {
        this.flow = flow;
        gson = new GsonBuilder().create();
    }
}
