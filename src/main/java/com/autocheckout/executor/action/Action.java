package com.autocheckout.executor.action;

import com.autocheckout.exception.BotException;

public interface Action {

    ActionResult perform() throws BotException;
}
