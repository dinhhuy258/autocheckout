package com.autocheckout.executor.action;

public class ActionResult<T> {

    private final boolean success;

    private final String message;

    private final T data;

    public static ActionResult fail() {
        return new ActionResult(false, null, null);
    }

    public static ActionResult fail(final String message) {
        return new ActionResult(false, message, null);
    }

    public static ActionResult success() {
        return new ActionResult(true, null, null);
    }

    public static ActionResult success(final Object data) {
        return new ActionResult(true, null, data);
    }

    private ActionResult(final boolean success, final String message, final T data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
