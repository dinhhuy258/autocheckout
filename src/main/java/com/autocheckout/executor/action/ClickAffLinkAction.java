package com.autocheckout.executor.action;

import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.flow.AbstractFlow;
import com.autocheckout.service.affiliate.AffiliateService;

public class ClickAffLinkAction extends AbstractAction {

    private final String affLink;

    private final AffiliateService affiliateService;

    public ClickAffLinkAction(final AbstractFlow flow, final String affLink) {
        super(flow);
        affiliateService = new AffiliateService(flow.getHttpConnector());
        this.affLink = affLink;
    }

    @Override
    public ActionResult perform() throws BotException {
        final String lazadaProductUrl = affiliateService.getProductUrlFromAffUrl(affLink.replaceAll(" ", "+"));
        flow.getHttpConnector().doGet(LazadaConstant.AFFILIATE_URL, lazadaProductUrl);
        return ActionResult.success();
    }
}
