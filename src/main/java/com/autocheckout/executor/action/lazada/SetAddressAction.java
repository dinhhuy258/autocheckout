/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.executor.action.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.ApiResponse;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.flow.lazada.CreateAccountFlow;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;
import com.autocheckout.executor.task.lazada.createaccount.CreateAccountTaskResult;

/**
 *
 * @author anhdtc-jp
 */
public class SetAddressAction extends LazadaAbstractAction {

    private final SubmitAddressInfo submitAddressInfo;

    public SetAddressAction(final LazadaAbstractFlow flow, final SubmitAddressInfo submitAddressInfo) {
        super(flow);
        this.submitAddressInfo = submitAddressInfo;
    }

    @Override
    public ActionResult perform() throws BotException {
        final ApiResponse apiResponse
            = gson.fromJson(flow.getHttpConnector().doPost(LazadaConstant.SUBMIT_ADDRESS, submitAddressInfo.toString(), flow.getCsrfToken()), ApiResponse.class);
        if (apiResponse == null || !apiResponse.getSuccess()) {
            LOG.debug("Create address fail");
            return ActionResult.fail("Tạo địa chỉ thất bại");
        }
        if (flow instanceof CreateAccountFlow) {
            final CreateAccountFlow createAccountFlow = (CreateAccountFlow) flow;
            return ActionResult.success(CreateAccountTaskResult.success(createAccountFlow.getEmail(), createAccountFlow.getPassword(), submitAddressInfo.showInfo()));
        }
        return ActionResult.success(CreateAccountTaskResult.success(null, null, submitAddressInfo.showInfo()));
    }
}
