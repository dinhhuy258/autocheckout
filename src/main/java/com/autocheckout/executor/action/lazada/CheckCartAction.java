package com.autocheckout.executor.action.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.ApiResponse;
import com.autocheckout.data.lazada.cart.CartItemData;
import com.autocheckout.data.lazada.cart.CartNum;
import com.autocheckout.data.lazada.cart.DeleteCartItem;
import com.autocheckout.data.lazada.cart.PageContext;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;
import com.autocheckout.http.HttpConnector;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class CheckCartAction extends LazadaAbstractAction {

    public CheckCartAction(final LazadaAbstractFlow flow) {
        super(flow);
    }

    @Override
    public ActionResult perform() throws BotException {
        final HttpConnector httpConnector = flow.getHttpConnector();
        final CartNum cartNum = gson.fromJson(httpConnector.doGet(LazadaConstant.CART_COUNT_ITEMS_URL), CartNum.class);
        if (cartNum != null && cartNum.getSuccess() && cartNum.getModule() != null && cartNum.getModule().getCartNum() > 0) {
            // In the case we already has product in cart
            final Document document = Jsoup.parse(httpConnector.doGet(LazadaConstant.CART_PAGE_URL));
            final String json = document.select("script:containsData(\"compress\":true)").first().dataNodes().get(0).getWholeData();
            final PageContext pageContext = gson.fromJson(json.substring(json.indexOf('{'), json.indexOf(';')), PageContext.class);
            final List<CartItemData> cartItemDataList = pageContext.getModule().getData().getCartItems();
            for (final CartItemData cartItemData : cartItemDataList) {
                final DeleteCartItem deleteCartItem = new DeleteCartItem();
                deleteCartItem.setCartItemData(cartItemData);
                deleteCartItem.setLinkage(pageContext.getModule().getLinkage());
                final ApiResponse apiResponse = gson.fromJson(httpConnector.doPost(LazadaConstant.CART_API_URL, deleteCartItem.toJson(gson), flow.getCsrfToken()), ApiResponse.class);
                if (apiResponse == null || !apiResponse.getSuccess()) {
                    // Got error while removing product from cart
                    LOG.debug("There is problem while removing product out of cart");
                    return ActionResult.fail("Có lỗi xảy ra khi xoá sản phẩm khỏi giỏ hàng");
                }
            }
        }
        System.out.println("CheckCart success");
        return ActionResult.success();
    }
}
