package com.autocheckout.executor.action.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.ApiResponse;
import com.autocheckout.data.lazada.account.AccountInfo;
import com.autocheckout.data.lazada.account.AddressInfo;
import com.autocheckout.data.lazada.account.SubmitAddressInfo;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;

public class UpdateAddressAction extends LazadaAbstractAction {

    private final SubmitAddressInfo newAddress;

    public UpdateAddressAction(final LazadaAbstractFlow flow, final SubmitAddressInfo newAddress) {
        super(flow);
        this.newAddress = newAddress;
    }

    @Override
    public ActionResult perform() throws BotException {
        final AccountInfo accountInfo = gson.fromJson(flow.getHttpConnector().doGet(LazadaConstant.USER_INFO_API_URL), AccountInfo.class);
        if (accountInfo.getModule().getHasAddress()) {
            // Update the exist address
            final AddressInfo addressInfo = gson.fromJson(flow.getHttpConnector().doGet(LazadaConstant.GET_ADDRESS_INFO_URL),
                AddressInfo.class);
            final String response = flow.getHttpConnector().doPost(LazadaConstant.UPDATE_ADDRESS_URL,
                newAddress.convertToUpdateAddressJson(addressInfo.getAddressId()), flow.getCsrfToken());
            final ApiResponse apiResponse = gson.fromJson(response,
                ApiResponse.class);
            if (apiResponse == null || !apiResponse.getSuccess()) {
                LOG.debug("There is problem while updating address for account");
                return ActionResult.fail("Có lỗi xảy ra khi cập nhật địa chỉ cho tài khoản");
            }

            System.out.println("Update addrss success 1");
            return ActionResult.success();
        } else {
            // create new one
            final ApiResponse apiResponse = gson.fromJson(flow.getHttpConnector().doPost(LazadaConstant.SUBMIT_ADDRESS,
                newAddress.toString(), flow.getCsrfToken()), ApiResponse.class);
            if (apiResponse == null || !apiResponse.getSuccess()) {
                LOG.debug("There is problem while creating address for account");
                return ActionResult.fail("Có lỗi xảy ra khi tạo địa chỉ cho tài khoản");
            }
            System.out.println("Update addrss success 2");
            return ActionResult.success();
        }
    }

}
