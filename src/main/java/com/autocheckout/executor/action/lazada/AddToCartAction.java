package com.autocheckout.executor.action.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.ApiResponse;
import com.autocheckout.data.lazada.cart.Cart;
import com.autocheckout.data.lazada.cart.CartItem;
import com.autocheckout.exception.BotException;
import com.autocheckout.exception.BussinessException;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;
import com.autocheckout.http.HttpConnector;
import com.autocheckout.service.affiliate.AffiliateService;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.impl.cookie.BasicClientCookie;

public class AddToCartAction extends LazadaAbstractAction {

    private final String productUrl;

    private final AffiliateService affiliateService;

    public AddToCartAction(final LazadaAbstractFlow flow, final String productUrl) {
        super(flow);
        this.productUrl = productUrl.replaceAll(" ", "+");
        affiliateService = new AffiliateService(flow.getHttpConnector());
    }

    @Override
    public ActionResult perform() throws BotException {
        final HttpConnector httpConnector = flow.getHttpConnector();
        final Cart cart;
        final String referer;
        if (!affiliateService.isAffUrl(productUrl)) {
            referer = productUrl;
            cart = createCart(productUrl);
        } else {
            final String lazadaProductUrl = affiliateService.getProductUrlFromAffUrl(productUrl);
            final List<BasicClientCookie> cookies = affiliateService.getAffiliateCookies(lazadaProductUrl);
            httpConnector.addCookies(cookies);
            httpConnector.doGet(LazadaConstant.AFFILIATE_URL, lazadaProductUrl);
            referer = lazadaProductUrl;
            cart = createCart(lazadaProductUrl);
        }
        final ApiResponse apiResponse = gson.fromJson(httpConnector.doPost(LazadaConstant.ADD_TO_CART_API_URL, gson.toJson(cart),
            flow.getCsrfToken(), referer), ApiResponse.class);
        if (apiResponse == null || !apiResponse.getSuccess()) {
            LOG.debug("Có lỗi xảy ra khi thêm sản phẩm vào giỏ hàng");
            return ActionResult.fail("Có lỗi xảy ra khi thêm sản phẩm vào giỏ hàng");
        }
        System.out.println("Add to cart success 1");
        return ActionResult.success();
    }

    private Cart createCart(final String lazadaProductUrl) throws BotException {
        Pattern pattern = Pattern.compile(".*?-i(\\d+)-s(\\d+).html.*");
        Matcher matcher = pattern.matcher(lazadaProductUrl);
        if (matcher.find()) {
            final Cart cart = new Cart();
            cart.add(new CartItem(matcher.group(1), matcher.group(2), 1));
            return cart;
        }
        pattern = Pattern.compile(".*?/products/i(\\d+)-s(\\d+).html.*");
        matcher = pattern.matcher(lazadaProductUrl);
        if (matcher.find()) {
            final Cart cart = new Cart();
            cart.add(new CartItem(matcher.group(1), matcher.group(2), 1));
            return cart;
        }
        LOG.debug("Product url is invalid: " + lazadaProductUrl);
        throw new BussinessException("Product url is invalid: " + lazadaProductUrl);
    }
}
