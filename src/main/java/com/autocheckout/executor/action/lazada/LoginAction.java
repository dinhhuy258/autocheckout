package com.autocheckout.executor.action.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.ApiResponse;
import com.autocheckout.data.lazada.account.Account;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;

public class LoginAction extends LazadaAbstractAction {

    private final Account account;

    public LoginAction(final LazadaAbstractFlow flow, final Account account) {
        super(flow);
        this.account = account;
    }

    @Override
    public ActionResult perform() throws BotException {
        final ApiResponse apiResponse = gson.fromJson(flow.getHttpConnector()
            .doPost(LazadaConstant.LOGIN_API_URL, gson.toJson(account), flow.getCsrfToken()), ApiResponse.class);
        if (apiResponse == null || !apiResponse.getSuccess()) {
            LOG.debug("Account is invalid! Email: " + account.getEmail() + " mật khẩu: " + account.getPassword());
            return ActionResult.fail("Tài khoản hoặc mật khẩu không hợp lệ! Email: " + account.getEmail() + " mật khẩu: " + account.getPassword());
        }
        System.out.println("Login success");
        return ActionResult.success();
    }

    public Account getAccount() {
        return account;
    }
}
