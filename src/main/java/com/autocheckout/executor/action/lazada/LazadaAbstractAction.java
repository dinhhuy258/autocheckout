package com.autocheckout.executor.action.lazada;

import com.autocheckout.executor.action.AbstractAction;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;

public abstract class LazadaAbstractAction extends AbstractAction<LazadaAbstractFlow>  {

    public LazadaAbstractAction(final LazadaAbstractFlow flow) {
        super(flow);
    }
}
