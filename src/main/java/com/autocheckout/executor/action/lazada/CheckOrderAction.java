package com.autocheckout.executor.action.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.orderdetail.OrderDetail;
import com.autocheckout.data.lazada.orderdetail.PageContext;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;
import com.autocheckout.executor.task.lazada.checkorder.CheckOrderTaskResult;
import com.google.gson.JsonParseException;
import java.util.Calendar;
import java.util.Date;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class CheckOrderAction extends LazadaAbstractAction {

    private final String buyerEmail;

    private final String orderId;

    public CheckOrderAction(final LazadaAbstractFlow flow, final String buyerEmail, final String orderId) {
        super(flow);
        this.buyerEmail = buyerEmail;
        this.orderId = orderId;
        addViLanguageCookie();
    }

    @Override
    public ActionResult perform() throws BotException {
        final String checkOrderUrl = String.format(LazadaConstant.CHECK_ORDER_URL, orderId, buyerEmail);
        final Document document = Jsoup.parse(flow.getHttpConnector().doGet(checkOrderUrl));
        final String data = document.select("script:containsData(window.__initData__)").first().dataNodes().get(0).getWholeData();
        final String json = data.substring(data.indexOf('{'), data.lastIndexOf('}') + 1).replaceAll("\\\\\"", "\"");
        try {
            final PageContext pageContext = gson.fromJson(json, PageContext.class);
            if (pageContext != null && pageContext.getSuccess() && pageContext.getData() != null) {
                return ActionResult.success(CheckOrderTaskResult.success(pageContext.getData()));
            }
            LOG.debug("Check order fail email: " + buyerEmail + " , order id: " + orderId);
            return ActionResult.fail("Kiểm tra đơn hàng thất bại");
        } catch (final Exception e) {
            if (e instanceof JsonParseException) {
                // Maybe the order has been canceled
                final OrderDetail orderDetail = new OrderDetail();
                orderDetail.setBuyerEmail(buyerEmail);
                orderDetail.setOrderId(orderId);
                orderDetail.setStatus("Đã bị huỷ");
                orderDetail.setNote("Đơn hàng đã bị huỷ, vui lòng kiểm tra lại. Nếu đơn hàng thực chất không phải bị huỷ xin vui lòng báo cáo lại người viết ứng dụng về lỗi này");
                return ActionResult.success(CheckOrderTaskResult.success(orderDetail));
            }
            LOG.debug("There is problem while checking order: " + e.getMessage());
            return ActionResult.fail("Có lỗi xảy ra trong quá trình kiểm tra đơn hàng");
        }
    }

    private void addViLanguageCookie() {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 1);
        final Date expiry = calendar.getTime();
        flow.getHttpConnector().addCookie("userLanguageML", "vi", "my.lazada.vn", "/", expiry);

    }
}
