/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.executor.action.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.ApiResponse;
import com.autocheckout.data.lazada.account.RegistrationAccountInfo;
import com.autocheckout.exception.BotException;
import com.autocheckout.executor.action.ActionResult;
import com.autocheckout.executor.flow.lazada.LazadaAbstractFlow;
import com.autocheckout.service.lazada.LazadaAccountService;
import java.util.Random;

/**
 *
 * @author anhdtc-jp
 */
public class CreateAccountAction extends LazadaAbstractAction {

    private final RegistrationAccountInfo account;

    private final LazadaAccountService lazadaAccountService;

    private final Random random;

    public CreateAccountAction(final LazadaAbstractFlow flow, final RegistrationAccountInfo account) {
        super(flow);
        this.account = account;
        lazadaAccountService = new LazadaAccountService(flow.getHttpConnector());
        random = new Random();
    }

    @Override
    public ActionResult perform() throws BotException {
        while (!lazadaAccountService.isAccountExist(account.getEmail())) {
            account.setEmail(account.getEmail() + random.nextInt(10));
        }
        final ApiResponse apiResponse = gson.fromJson(flow.getHttpConnector().doPost(LazadaConstant.REG_ACC_API_LINK, account.convertToJson(), flow.getCsrfToken()), ApiResponse.class);
        if (apiResponse == null || !apiResponse.getSuccess()) {
            LOG.debug("Create account fail");
            return ActionResult.fail("Tạo tài khoản thất bại");
        }
        return ActionResult.success();
    }
}
