package com.autocheckout.parser;

import com.autocheckout.LOG;
import com.autocheckout.data.lazada.checkout.PageContext;
import com.autocheckout.exception.BotException;
import com.autocheckout.http.HttpConnector;
import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class PageContextParser {

    private final HttpConnector httpConnector;

    private final Gson gson;

    public PageContextParser(final HttpConnector httpConnector, final Gson gson) {
        this.httpConnector = httpConnector;
        this.gson = gson;
    }

    public PageContext parse(final String url) throws BotException {
        String res = httpConnector.doGet(url);
        LOG.debug("Parse Url :" + url);
        //  LOG.debug("Parse res :" + res);
        final Document document = Jsoup.parse(res);
        final String json = document.select("script:containsData(\"compress\":true)").first().dataNodes().get(0).getWholeData();
        LOG.debug("Parse res :" + json);
        final PageContext pageContext = gson.fromJson(json.substring(json.indexOf('{'), json.indexOf(';')), PageContext.class);
        return pageContext;
    }
}
