package com.autocheckout.parser;

public class ProxyParser {

    private static final String PROXY_SEPERATOR = "/";

    private final String proxyHost;

    private final int proxyPort;

    private String proxyUser;

    private String proxyPass;

    private boolean hasAuthentication;

    public ProxyParser(final String proxy) {
        final String[] proxyValue = proxy.split(PROXY_SEPERATOR);
        proxyHost = proxyValue[0];
        proxyPort = Integer.parseInt(proxyValue[1]);
        if (proxyValue.length == 4) {
            proxyUser = proxyValue[2];
            proxyPass = proxyValue[3];
            hasAuthentication = true;
        }
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public String getProxyUser() {
        return proxyUser;
    }

    public String getProxyPass() {
        return proxyPass;
    }

    public boolean hasAuthentication() {
        return hasAuthentication;
    }
}
