package com.autocheckout.data.lazada.checkout.payment;

public class PaymentMethodOption {

    private String id;

    private String title;

    private String subtitle;

    private String icon;

    private String serviceOption;

    private String disable;

    private String hidden;

    private String tip;

    private Integer sequence;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(final String subtitle) {
        this.subtitle = subtitle;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(final String icon) {
        this.icon = icon;
    }

    public String getServiceOption() {
        return serviceOption;
    }

    public void setServiceOption(final String serviceOption) {
        this.serviceOption = serviceOption;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(final String disable) {
        this.disable = disable;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(final String hidden) {
        this.hidden = hidden;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(final String tip) {
        this.tip = tip;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(final Integer sequence) {
        this.sequence = sequence;
    }
}
