package com.autocheckout.data.lazada.checkout.payment;

import java.util.List;

public class PaymentMethodValidate {

    private List<PaymentMethodValidateSelectedId> selectedId;

    public List<PaymentMethodValidateSelectedId> getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(final List<PaymentMethodValidateSelectedId> selectedId) {
        this.selectedId = selectedId;
    }
}
