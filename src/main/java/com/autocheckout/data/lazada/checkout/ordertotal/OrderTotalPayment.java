package com.autocheckout.data.lazada.checkout.ordertotal;

public class OrderTotalPayment {

    private String pay;

    private String taxTip;

    private String title;

    public String getPay() {
        return pay;
    }

    public void setPay(final String pay) {
        this.pay = pay;
    }

    public String getTaxTip() {
        return taxTip;
    }

    public void setTaxTip(final String taxTip) {
        this.taxTip = taxTip;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }
}
