package com.autocheckout.data.lazada.checkout.payment;

public class PaymentMethodList {

    private PaymentMethodFields fields;

    private String id;

    private String tag;

    private String type;

    private PaymentMethodValidate validate;

    public PaymentMethodFields getFields() {
        return fields;
    }

    public void setFields(final PaymentMethodFields fields) {
        this.fields = fields;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(final String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public PaymentMethodValidate getValidate() {
        return validate;
    }

    public void setValidate(final PaymentMethodValidate validate) {
        this.validate = validate;
    }
}
