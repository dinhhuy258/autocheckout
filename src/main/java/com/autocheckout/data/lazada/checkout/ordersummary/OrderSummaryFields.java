package com.autocheckout.data.lazada.checkout.ordersummary;

import java.util.List;

public class OrderSummaryFields {

    private List<OrderSummaryInfo> summarys;

    public List<OrderSummaryInfo> getSummarys() {
        return summarys;
    }

    public void setSummarys(final List<OrderSummaryInfo> summarys) {
        this.summarys = summarys;
    }
}
