package com.autocheckout.data.lazada.checkout.payment;

import java.util.List;

public class PaymentMethodFields {

    private List<PaymentMethodOption> options;

    private String payerId;

    private String selectedId;

    private String userType;

    public List<PaymentMethodOption> getPaymentMethodOptions() {
        return options;
    }

    public void setPaymentMethodOptions(final List<PaymentMethodOption> paymentMethodOptions) {
        options = paymentMethodOptions;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(final String payerId) {
        this.payerId = payerId;
    }

    public String getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(final String selectedId) {
        this.selectedId = selectedId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(final String userType) {
        this.userType = userType;
    }
}
