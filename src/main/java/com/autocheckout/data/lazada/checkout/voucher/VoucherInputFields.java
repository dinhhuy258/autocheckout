package com.autocheckout.data.lazada.checkout.voucher;

public class VoucherInputFields {

    private String buttonText;

    private String placeHolder;

    private String status;

    private String value;

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(final String buttonText) {
        this.buttonText = buttonText;
    }

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(final String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
