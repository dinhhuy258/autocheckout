package com.autocheckout.data.lazada.checkout.ordertotal;

public class OrderTotal {

    private OrderTotalFields fields;

    private String id;

    private String tag;

    private String type;

    public OrderTotalFields getFields() {
        return fields;
    }

    public void setFields(final OrderTotalFields fields) {
        this.fields = fields;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(final String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }
}
