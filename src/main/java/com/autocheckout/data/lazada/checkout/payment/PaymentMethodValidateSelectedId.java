package com.autocheckout.data.lazada.checkout.payment;

public class PaymentMethodValidateSelectedId {

    private String regex;

    private String msg;

    public String getRegex() {
        return regex;
    }

    public void setRegex(final String regex) {
        this.regex = regex;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(final String msg) {
        this.msg = msg;
    }
}
