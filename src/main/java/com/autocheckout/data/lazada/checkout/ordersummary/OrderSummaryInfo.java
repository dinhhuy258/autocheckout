package com.autocheckout.data.lazada.checkout.ordersummary;

public class OrderSummaryInfo {

    private String title;

    private String value;

    public void setTitle(final String title) {
        this.title = title;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }
}
