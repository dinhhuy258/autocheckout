package com.autocheckout.data.lazada.checkout.placeorder;

public class PlaceOrderFields {

    private String checkFlow;

    private String isSubmit;

    private Integer paymentRetry;

    private Integer timeLimit;

    private Long timeStamp;

    private String title;

    private String userType;

    public String getCheckFlow() {
        return checkFlow;
    }

    public void setCheckFlow(final String checkFlow) {
        this.checkFlow = checkFlow;
    }

    public String getIsSubmit() {
        return isSubmit;
    }

    public void setIsSubmit(final String isSubmit) {
        this.isSubmit = isSubmit;
    }

    public Integer getPaymentRetry() {
        return paymentRetry;
    }

    public void setPaymentRetry(final Integer paymentRetry) {
        this.paymentRetry = paymentRetry;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(final Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(final Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(final String userType) {
        this.userType = userType;
    }
}
