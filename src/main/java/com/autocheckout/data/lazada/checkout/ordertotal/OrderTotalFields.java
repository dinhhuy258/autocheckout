package com.autocheckout.data.lazada.checkout.ordertotal;

public class OrderTotalFields {

    private OrderTotalButton button;

    private OrderTotalPayment payment;

    private Long timestamp;

    public OrderTotalButton getButton() {
        return button;
    }

    public void setButton(final OrderTotalButton button) {
        this.button = button;
    }

    public OrderTotalPayment getPayment() {
        return payment;
    }

    public void setPayment(final OrderTotalPayment payment) {
        this.payment = payment;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Long timestamp) {
        this.timestamp = timestamp;
    }
}
