package com.autocheckout.data.lazada.checkout.ordertotal;

public class OrderTotalButton {

    private String actionDialog;

    private String actionUrl;

    private Boolean clicked;

    private Boolean enable;

    private String text;

    private String textColor;

    public String getActionDialog() {
        return actionDialog;
    }

    public void setActionDialog(final String actionDialog) {
        this.actionDialog = actionDialog;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(final String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public Boolean getClicked() {
        return clicked;
    }

    public void setClicked(final Boolean clicked) {
        this.clicked = clicked;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(final Boolean enable) {
        this.enable = enable;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(final String textColor) {
        this.textColor = textColor;
    }
}
