/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.data.lazada.account;

import com.autocheckout.LOG;

/**
 *
 * @author anhdtc-jp
 */
public class RegistrationAccountInfo {

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(final String repassword) {
        this.repassword = repassword;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public boolean isEnableNewsletterprivate() {
        return enableNewsletterprivate;
    }

    public void setEnableNewsletterprivate(final boolean enableNewsletterprivate) {
        this.enableNewsletterprivate = enableNewsletterprivate;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(final String month) {
        this.month = month;
    }

    public String getDayprivate() {
        return dayprivate;
    }

    public void setDayprivate(final String dayprivate) {
        this.dayprivate = dayprivate;
    }

    public String getYearprivate() {
        return yearprivate;
    }

    public void setYearprivate(final String yearprivate) {
        this.yearprivate = yearprivate;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(final String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(final String gender) {
        this.gender = gender;
    }

    public String getLoading() {
        return loading;
    }

    public void setLoading(final String loading) {
        this.loading = loading;
    }

    public RegistrationAccountInfo() {

        email = "alalalala32132@gmail.com";
        password = "abcd123";
        repassword = "abcd123";
        name = "anh anh";
        enableNewsletterprivate = true;
        month = "1";
        dayprivate = "1";
        yearprivate = "2016";
        birthday = "2016-1-1";
        gender = "male";
        loading = "false";
    }

    private String email;// = "ansnsn@gmail.com";
    private String password;// = "abcd123";
    private String repassword;// = "abcd123";
    private String name;// = "anh anh";
    private boolean enableNewsletterprivate;// = true;
    private String month;// = "1";
    private String dayprivate;// = "1";
    private String yearprivate;// = "2016";
    private String birthday;// = "2016-1-1";
    private String gender;// = "male";
    private String loading;// = "false";

    public RegistrationAccountInfo(final String email, final String password, final String repassword, final String name, final String month,
        final String dayprivate, final String yearprivate, final String birthday, final String gender) {
        this.email = email;
        this.password = password;
        this.repassword = repassword;
        this.name = name;
        enableNewsletterprivate = true;
        this.month = month;
        this.dayprivate = dayprivate;
        this.yearprivate = yearprivate;
        this.birthday = birthday;
        this.gender = gender;
        loading = "false";
    }

    public String convertToJson() {
        final String result = "{\"email\":\"" + email + "\","
            + "\"password\":\"" + password + "\","
            + "\"re-password\":\"" + repassword + "\","
            + "\"name\":\"" + name + "\","
            + "\"enableNewsletter\":" + enableNewsletterprivate + ","
            + "\"month\":\"" + month + "\","
            + "\"day\":" + dayprivate + ","
            + "\"year\":" + yearprivate + ","
            + "\"birthday\":\"" + birthday + "\","
            + "\"gender\":\"" + gender + "\","
            + "\"loading\":\"" + loading + ""
            + "\"}";
        LOG.debug("Json" + result);
        return result;
    }

    public String convertToSave() {
        final String result = email + "/" + password;
        return result;
    }
}
