/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.data.lazada.account;

/**
 *
 * @author anhdtc-jp
 */
public class AddressApiResponse {

    public class Module {

        private String id;

        private String parentId;

        private String name;

        private String displayName;

        private String nameLocal;

        public String getId() {
            return id;
        }

        public void setId(final String id) {
            this.id = id;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(final String parentId) {
            this.parentId = parentId;
        }

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(final String displayName) {
            this.displayName = displayName;
        }

        public String getNameLocal() {
            return nameLocal;
        }

        public void setNameLocal(final String nameLocal) {
            this.nameLocal = nameLocal;
        }

        @Override
        public String toString() {
            return id + "/" + parentId + "/" + name + "/" + displayName + "/" + nameLocal;
        }
    }

    private int type;

    public int getType() {
        return type;
    }

    public void setType(final int type) {
        this.type = type;
    }

    private Module[] module;

    private int errorCode;

    private String notSuccess;

    private boolean success;

    public Module[] getModule() {
        return module;
    }

    public void setModule(final Module[] module) {
        this.module = module;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(
        final int errorCode) {
        this.errorCode = errorCode;
    }

    public String getNotSuccess() {
        return notSuccess;
    }

    public void setNotSuccess(final String notSuccess) {
        this.notSuccess = notSuccess;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "AddressApiResponse [module = " + module + ", errorCode = "
            + errorCode + ", notSuccess = " + notSuccess + ", success = " + success + "]";
    }

    public String convertToSave() {

//                ("type / id / parentId / name / displayName / nameLocal" + '\n');
        return type + "/" + toString();
    }
}
