/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.data.lazada.account;

/**
 *
 * @author anhdtc-jp
 */
public class CheckMailApiResponse {

    public class Module {

        private String result;

        public String getResult() {
            return result;
        }

        public void setResult(final String result) {
            this.result = result;
        }

        @Override
        public String toString() {
            return " [result = " + result + "]";
        }
    }
    private Module module;

    private int errorCode;

    private String notSuccess;

    private boolean success;

    public Module getModule() {
        return module;
    }

    public void setModule(final Module module) {
        this.module = module;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(final int errorCode) {
        this.errorCode = errorCode;
    }

    public String getNotSuccess() {
        return notSuccess;
    }

    public void setNotSuccess(final String notSuccess) {
        this.notSuccess = notSuccess;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "ClassPojo [module = " + module + ", errorCode = " + errorCode + ", notSuccess = " + notSuccess + ", success = " + success + "]";
    }
}
