/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.data.lazada.account;

import com.autocheckout.LOG;

/**
 *
 * @author anhdtc-jp
 */
public class SubmitAddressInfo {

    private String phone;

    private String cityId;
    private String cityName;
    private String prefectureId;
    private String prefectureName;
    private String townId;
    private String townName;
    private String name;
    private String detailAddress;

    public SubmitAddressInfo(final String phone, final String cityId, final String cityName, final String prefectureId, final String prefectureName, final String townId, final String townName, final String name, final String detailAddress) {
        this.phone = phone;
        this.cityId = cityId;
        this.cityName = cityName;
        this.prefectureId = prefectureId;
        this.prefectureName = prefectureName;
        this.townId = townId;
        this.townName = townName;
        this.name = name;
        this.detailAddress = detailAddress;
        LOG.debug("new SubmitAddressInfo" + toString());
    }

    public SubmitAddressInfo(final Address townAddress, final String name, final String phoneNumber) {

        phone = phoneNumber;
        this.name = name;

        switch (townAddress.getType()) {

            // address is City
            case 0:
                cityId = townAddress.getAddressID();
                cityName = townAddress.getAddressName();
                prefectureId = "";
                prefectureName = "";
                townId = "";
                townName = "";
                detailAddress = "";
                break;

            // address is prpefecture
            case 1:
                cityId = townAddress.getParentAddress().getAddressID();
                cityName = townAddress.getParentAddress().getAddressName();
                prefectureId = townAddress.getAddressID();
                prefectureName = townAddress.getAddressName();
                townId = "";
                townName = "";
                detailAddress = "";
                break;

            // address is town
            case 2:
                cityId = townAddress.getParentAddress().getParentAddress().getAddressID();
                cityName = townAddress.getParentAddress().getParentAddress().getAddressName();
                prefectureId = townAddress.getParentAddress().getAddressID();
                prefectureName = townAddress.getParentAddress().getAddressName();
                townId = townAddress.getAddressID();
                townName = townAddress.getAddressName();
                detailAddress = "";
                break;

            // address have details infor
            case 3:
                cityId = townAddress.getParentAddress().getParentAddress().getAddressID();
                cityName = townAddress.getParentAddress().getParentAddress().getAddressName();
                prefectureId = townAddress.getParentAddress().getAddressID();
                prefectureName = townAddress.getParentAddress().getAddressName();
                townId = townAddress.getAddressID();
                townName = townAddress.getAddressName();
                detailAddress = townAddress.getAddressDetail();
                break;
            default:

                cityId = "";
                cityName = "";
                prefectureId = "";
                prefectureName = "";
                townId = "";
                townName = "";
                detailAddress = "";
                break;
        }
        LOG.debug("New SubmitAddressInfo with address Type *" + townAddress.getType() + " and phone" + toString());
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(final String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(final String cityName) {
        this.cityName = cityName;
    }

    public String getPrefectureId() {
        return prefectureId;
    }

    public void setPrefectureId(final String prefectureId) {
        this.prefectureId = prefectureId;
    }

    public String getPrefectureName() {
        return prefectureName;
    }

    public void setPrefectureName(final String prefectureName) {
        this.prefectureName = prefectureName;
    }

    public String getTownId() {
        return townId;
    }

    public void setTownId(final String townId) {
        this.townId = townId;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(final String townName) {
        this.townName = townName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(final String detailAddress) {
        this.detailAddress = detailAddress;
    }

    @Override
    public String toString() {
        final String jsonresult = "{\"name\":\"" + name + "\","
            + "\"phone\":\"" + phone + "\","
            + "\"detailAddress\":\"" + detailAddress + "\","
            + "\"locationTreeAddressArray\":"
            + "\"["
            + "{"
            + "\\\"id\\\":\\\"" + cityId + "\\\","
            + "\\\"name\\\":\\\"" + cityName + "\\\""
            + "},"
            + "{"
            + "\\\"id\\\":\\\"" + prefectureId + "\\\","
            + "\\\"name\\\":\\\"" + prefectureName + "\\\""
            + "},"
            + "{"
            + "\\\"id\\\":\\\"" + townId + "\\\","
            + "\\\"name\\\":\\\"" + townName + "\\\""
            + "}"
            + "]\","
            + "\"locationTreeAddressId\":\"" + cityId + "-" + prefectureId + "-" + townId + "\","
            + "\"locationTreeAddressName\":\"" + cityName + "," + prefectureName + "," + townName + "\""
            + "}";
        return jsonresult;
    }

    public String showInfo() {
        return String.join(",", name, phone, cityName, prefectureName, townName, detailAddress);
    }

    public String convertToUpdateAddressJson(final String addressId) {
        final String jsonresult = "{\"name\":\"" + name + "\","
            + "\"phone\":\"" + phone + "\","
            + "\"detailAddress\":\"" + detailAddress + "\","
            + "\"locationTreeAddressArray\":"
            + "\"["
            + "{"
            + "\\\"id\\\":\\\"" + cityId + "\\\","
            + "\\\"name\\\":\\\"" + cityName + "\\\""
            + "},"
            + "{"
            + "\\\"id\\\":\\\"" + prefectureId + "\\\","
            + "\\\"name\\\":\\\"" + prefectureName + "\\\""
            + "},"
            + "{"
            + "\\\"id\\\":\\\"" + townId + "\\\","
            + "\\\"name\\\":\\\"" + townName + "\\\""
            + "}"
            + "]\","
            + "\"locationTreeAddressId\":\"" + cityId + "-" + prefectureId + "-" + townId + "\","
            + "\"locationTreeAddressName\":\"" + cityName + "," + prefectureName + "," + townName + "\","
            + "\"addressId\":\"" + addressId + "\""
            + "}";
        return jsonresult;
    }
}
