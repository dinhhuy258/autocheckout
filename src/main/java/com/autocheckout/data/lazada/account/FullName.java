/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.data.lazada.account;

/**
 *
 * @author anhdtc-jp
 */
public class FullName {

    private String firstName;
    private String subName;
    private String lastName;
    private String fullName;

    public FullName(final String firstName, final String subName, final String lastName, final String fullName) {
        this.firstName = firstName;
        this.subName = subName;
        this.lastName = lastName;
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(final String subName) {
        this.subName = subName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

}
