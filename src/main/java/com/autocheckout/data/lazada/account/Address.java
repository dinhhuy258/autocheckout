/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.data.lazada.account;

/**
 *
 * @author anhdtc-jp
 */
public class Address {

    private int type;

    private Address parentAddress;

    private String addressID;
    private String addressName;
    private String displayName;
    private String nameLocal;
    private String addressDetail;

    public Address(final Address source) {
        type = source.type;
        parentAddress = source.parentAddress;
        addressID = source.addressID;
        addressName = source.addressName;
        displayName = source.displayName;
        nameLocal = source.nameLocal;
        addressDetail = source.addressDetail;
    }

    public Address(final int type, final Address parentAddress, final String addressID, final String addressName,
        final String displayName, final String nameLocal, final String addressDetail) {
        this.type = type;
        this.parentAddress = parentAddress;
        this.addressID = addressID;
        this.addressName = addressName;
        this.displayName = displayName;
        this.nameLocal = nameLocal;
        this.addressDetail = addressDetail;
    }

    public Address(final int type, final Address parentAddress, final String addressID, final String addressName,
        final String displayName, final String nameLocal) {
        this.type = type;
        this.parentAddress = parentAddress;
        this.addressID = addressID;
        this.addressName = addressName;
        this.displayName = displayName;
        this.nameLocal = nameLocal;
        addressDetail = "";
    }

    @Override
    public String toString() {
        return parentAddress + ", addressName=" + addressName + " " + addressDetail;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(final String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public int getType() {
        return type;
    }

    public void setType(final int type) {
        this.type = type;
    }

    public Address getParentAddress() {
        return parentAddress;
    }

    public void setParentAddress(final Address parentAddress) {
        this.parentAddress = parentAddress;
    }

    public String getAddressID() {
        return addressID;
    }

    public void setAddressID(final String addressID) {
        this.addressID = addressID;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(final String addressName) {
        this.addressName = addressName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    public String getNameLocal() {
        return nameLocal;
    }

    public void setNameLocal(final String nameLocal) {
        this.nameLocal = nameLocal;
    }
}
