package com.autocheckout.data.lazada.account;

import com.autocheckout.deserializer.AddressInfoDeserializer;
import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(AddressInfoDeserializer.class)
public class AddressInfo {

    private String addressId;

    private String name;

    private String phone;

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(final String addressId) {
        this.addressId = addressId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }
}
