package com.autocheckout.data.lazada.account;

public class AccountInfo {

    public class Module {

        private Boolean hasAddress;

        private String name;

        public Boolean getHasAddress() {
            return hasAddress;
        }

        public void setHasAddress(final Boolean hasAddress) {
            this.hasAddress = hasAddress;
        }

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }
    }

    private Module module;

    private Boolean success;

    public Module getModule() {
        return module;
    }

    public void setModule(final Module module) {
        this.module = module;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(final Boolean success) {
        this.success = success;
    }
}
