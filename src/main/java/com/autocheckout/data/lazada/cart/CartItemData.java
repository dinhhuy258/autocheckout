package com.autocheckout.data.lazada.cart;

public class CartItemData {

    private String id;

    private String tag;

    private String type;

    private CartItemDataFields fields;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(final String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public CartItemDataFields getFields() {
        return fields;
    }

    public void setFields(final CartItemDataFields fields) {
        this.fields = fields;
    }
}
