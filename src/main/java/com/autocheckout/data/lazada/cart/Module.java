package com.autocheckout.data.lazada.cart;

public class Module {

    private Data data;

    private Linkage linkage;

    public Data getData() {
        return data;
    }

    public void setData(final Data data) {
        this.data = data;
    }

    public Linkage getLinkage() {
        return linkage;
    }

    public void setLinkage(final Linkage linkage) {
        this.linkage = linkage;
    }
}
