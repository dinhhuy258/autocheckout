package com.autocheckout.data.lazada.cart;

public class PageContext {

    private Module module;

    public Module getModule() {
        return module;
    }

    public void setModule(final Module module) {
        this.module = module;
    }
}
