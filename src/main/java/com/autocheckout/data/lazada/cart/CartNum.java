package com.autocheckout.data.lazada.cart;

public class CartNum {

    public class Module {

        private Integer cartNum;

        public Integer getCartNum() {
            return cartNum;
        }

        public void setCartNum(final Integer cartNum) {
            this.cartNum = cartNum;
        }
    }

    private Boolean success;

    private Module module;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(final Boolean success) {
        this.success = success;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(final Module module) {
        this.module = module;
    }
}
