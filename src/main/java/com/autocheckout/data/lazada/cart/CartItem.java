package com.autocheckout.data.lazada.cart;

public class CartItem {

    private String itemId;

    private String skuId;

    private Integer quantity;

    public CartItem(final String itemId, final String skuId, final Integer quantity) {
        this.itemId = itemId;
        this.skuId = skuId;
        this.quantity = quantity;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(final String itemId) {
        this.itemId = itemId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(final String skuId) {
        this.skuId = skuId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }
}
