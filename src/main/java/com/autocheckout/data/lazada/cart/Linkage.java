package com.autocheckout.data.lazada.cart;

public class Linkage {

    private Common common;

    private String signature;

    public Common getCommon() {
        return common;
    }

    public void setCommon(final Common common) {
        this.common = common;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(final String signature) {
        this.signature = signature;
    }
}
