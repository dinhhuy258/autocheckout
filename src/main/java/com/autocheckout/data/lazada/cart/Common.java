package com.autocheckout.data.lazada.cart;

public class Common {

    private Boolean compress;

    private String queryParams;

    public Boolean getCompress() {
        return compress;
    }

    public void setCompress(final Boolean compress) {
        this.compress = compress;
    }

    public String getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(final String queryParams) {
        this.queryParams = queryParams;
    }
}
