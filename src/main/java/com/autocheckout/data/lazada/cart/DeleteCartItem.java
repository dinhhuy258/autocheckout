package com.autocheckout.data.lazada.cart;

import com.google.gson.Gson;

public class DeleteCartItem {

    private String operator;

    private CartItemData cartItemData;

    private Linkage linkage;

    public String getOperator() {
        return operator;
    }

    public void setOperator(final String operator) {
        this.operator = operator;
    }

    public CartItemData getCartItemData() {
        return cartItemData;
    }

    public void setCartItemData(final CartItemData cartItemData) {
        this.cartItemData = cartItemData;
        operator = "item_" + cartItemData.getId();
    }

    public Linkage getLinkage() {
        return linkage;
    }

    public void setLinkage(final Linkage linkage) {
        this.linkage = linkage;
    }

    public String toJson(final Gson gson) {
        final String linkageJson = gson.toJson(linkage);
        return "{" +
                "\"operator\":" + "\"" + operator + "\"," +
                "\"data\":" + "{" +
                            "\"" + operator + "\":" + "{" +
                                 "\"id\":" + "\"" + cartItemData.getId() + "\"," +
                                 "\"tag\":" + "\"" + cartItemData.getTag() + "\"," +
                                 "\"type\":" + "\"" + cartItemData.getType() + "\"," +
                                 "\"fields\":" + "{" +
                                    "\"cartItemId\":" + cartItemData.getFields().getCartItemId() + "," +
                                    "\"operation\":" + "\"" + cartItemData.getFields().getOperation() + "\"" +
                                "}" +
                            "}" +
                "}," +

            "\"linkage\":" + linkageJson +
        "}";


    }
}
