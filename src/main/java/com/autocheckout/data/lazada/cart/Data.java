package com.autocheckout.data.lazada.cart;

import java.util.List;

import com.autocheckout.deserializer.CartDataDeserializer;
import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(CartDataDeserializer.class)
public class Data {

    List<CartItemData> cartItems;

    public List<CartItemData> getCartItems() {
        return cartItems;
    }

    public void setCartItems(final List<CartItemData> cartItems) {
        this.cartItems = cartItems;
    }
}
