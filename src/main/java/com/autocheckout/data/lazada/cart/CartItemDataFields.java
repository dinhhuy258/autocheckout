package com.autocheckout.data.lazada.cart;

public class CartItemDataFields {

    private String cartItemId;

    private String operation;

    public CartItemDataFields() {
        operation = "delete";
    }

    public String getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(final String cartItemId) {
        this.cartItemId = cartItemId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(final String operation) {
        this.operation = operation;
    }
}
