package com.autocheckout.data.lazada;

import com.google.gson.annotations.SerializedName;

public class ApiResponse {

    public class ErrorCode {

        private String displayMessage;

        private String key;

        private String logMessage;

        public String getDisplayMessage() {
            return displayMessage;
        }

        public void setDisplayMessage(final String displayMessage) {
            this.displayMessage = displayMessage;
        }

        public String getKey() {
            return key;
        }

        public void setKey(final String key) {
            this.key = key;
        }

        public String getLogMessage() {
            return logMessage;
        }

        public void setLogMessage(final String logMessage) {
            this.logMessage = logMessage;
        }

        @Override
        public String toString() {
            return "ClassPojo [displayMessage = " + displayMessage + ", key = " + key + ", logMessage = " + logMessage + "]";
        }
    }

    public class Module {

        public class Data {
            public class PlaceOrderResultAction {

                public class Fields {

                    public class FieldsData {

                        private String orderId;

                        public String getOrderId() {
                            return orderId;
                        }

                        public void setOrderId(final String orderId) {
                            this.orderId = orderId;
                        }
                    }

                    private FieldsData data;

                    private String result;

                    public FieldsData getData() {
                        return data;
                    }

                    public void setData(final FieldsData data) {
                        this.data = data;
                    }

                    public String getResult() {
                        return result;
                    }

                    public void setResult(final String result) {
                        this.result = result;
                    }
                }

                private Fields fields;

                public Fields getFields() {
                    return fields;
                }

                public void setFields(final Fields fields) {
                    this.fields = fields;
                }
            }


            public class VoucherInputResultAction {

                public class Fields {

                    private String status;

                    public String getStatus() {
                        return status;
                    }

                    public void setStatus(final String status) {
                        this.status = status;
                    }
                }

                private Fields fields;

                public Fields getFields() {
                    return fields;
                }

                public void setFields(final Fields fields) {
                    this.fields = fields;
                }
            }

            @SerializedName("placeOrderResultAction_10004")
            private PlaceOrderResultAction placeOrderResultAction;

            @SerializedName("voucherInput_1")
            private VoucherInputResultAction voucherInputResultAction;

            public PlaceOrderResultAction getPlaceOrderResultAction() {
                return placeOrderResultAction;
            }

            public void setPlaceOrderResultAction(final PlaceOrderResultAction placeOrderResultAction) {
                this.placeOrderResultAction = placeOrderResultAction;
            }

            public VoucherInputResultAction getVoucherInputResultAction() {
                return voucherInputResultAction;
            }

            public void setVoucherInputResultAction(final VoucherInputResultAction voucherInputResultAction) {
                this.voucherInputResultAction = voucherInputResultAction;
            }
        }

        private Data data;

        private String result;

        private Boolean isValid;

        public Data getData() {
            return data;
        }

        public void setData(final Data data) {
            this.data = data;
        }

        public Boolean getIsValid() {
            return isValid;
        }

        public void setIsValid(final Boolean isValid) {
            this.isValid = isValid;
        }

        public String getResult() {
            return result;
        }

        public void setResult(final String result) {
            this.result = result;
        }

        @Override
        public String toString() {
            return " [result = " + result + "]";
        }
    }

    private Module module;

    public Module getModule() {
        return module;
    }

    public void setModule(final Module module) {
        this.module = module;
    }

    private ErrorCode errorCode;

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(final ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(final Boolean success) {
        this.success = success;
    }

}
