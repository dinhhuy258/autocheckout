package com.autocheckout.data.lazada.orderdetail;

import com.autocheckout.deserializer.OrderDetailDeserializer;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

public class PageContext {
    @SerializedName("module")
    @JsonAdapter(OrderDetailDeserializer.class)
    private OrderDetail data;

    private Boolean success;

    public OrderDetail getData() {
        return data;
    }

    public void setData(final OrderDetail data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(final Boolean success) {
        this.success = success;
    }
}
