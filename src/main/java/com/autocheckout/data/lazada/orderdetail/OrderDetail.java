package com.autocheckout.data.lazada.orderdetail;

public class OrderDetail {

    private String status;

    private String orderId;

    private String buyerName;

    private String buyerEmail;

    private String phone;

    private String region;

    private String address;

    private String createAt;

    private String deliveryDate;

    private String productName;

    private String productLink;

    private String price;

    private String note;

    private String logisticPartner;

    private String trackingNumber;

    public OrderDetail() {
        status = "";
        orderId = "";
        buyerName = "";
        buyerEmail = "";
        phone = "";
        region = "";
        address = "";
        createAt = "";
        deliveryDate = "";
        productName = "";
        productLink = "";
        price = "";
        note = "";
        logisticPartner = "";
        trackingNumber = "";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(final String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(final String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(final String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(final String createAt) {
        this.createAt = createAt;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(final String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(final String productName) {
        this.productName = productName;
    }

    public String getProductLink() {
        return productLink;
    }

    public void setProductLink(final String productLink) {
        this.productLink = productLink;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(final String note) {
        this.note = note;
    }

    public String getLogisticPartner() {
        return logisticPartner;
    }

    public void setLogisticPartner(final String logisticPartner) {
        this.logisticPartner = logisticPartner;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(final String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
}
