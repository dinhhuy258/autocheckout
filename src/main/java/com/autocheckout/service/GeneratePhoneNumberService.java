/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.service;

import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.util.FileUtils;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author anhdtc-jp
 */
public class GeneratePhoneNumberService {

    private static ArrayList<String> PHONE_BOOK_DATA;

    private static ArrayList<String> WORKING_PHONE_BOOK;

    static {
        WORKING_PHONE_BOOK = FileUtils.importFile(LazadaConstant.FILE_PATH.PHONE_BOOK_PATH);

        PHONE_BOOK_DATA = new ArrayList<>(WORKING_PHONE_BOOK);
    }

    public GeneratePhoneNumberService() {

    }

    public static ArrayList<String> getPHONE_BOOK() {
        return PHONE_BOOK_DATA;
    }

    public static boolean setPHONE_BOOK(ArrayList<String> PHONE_BOOK) {

        if (PHONE_BOOK.size() < 1) {
            return false;
        }

        GeneratePhoneNumberService.WORKING_PHONE_BOOK = PHONE_BOOK;
        PHONE_BOOK_DATA = new ArrayList<>(PHONE_BOOK);
        return true;
    }

    /**
     *
     * @param numberOfPhonenumber
     * @return List of phone number.
     */
    public static ArrayList<String> generatePhoneNumber(int numberOfPhonenumber) {

        Random ran = new Random();

        ArrayList<String> rs = new ArrayList<>();

        int phoneBookSize = WORKING_PHONE_BOOK.size();

        for (int i = 0; i < numberOfPhonenumber; i++) {

            if (phoneBookSize < 1) {
                WORKING_PHONE_BOOK = new ArrayList<>(PHONE_BOOK_DATA);
                phoneBookSize = WORKING_PHONE_BOOK.size();
            }

            int index = ran.nextInt(phoneBookSize--);

            rs.add(WORKING_PHONE_BOOK.get(index));
            WORKING_PHONE_BOOK.remove(index);
        }
        return rs;
    }
}
