package com.autocheckout.service.affiliate;

import com.autocheckout.LOG;
import com.autocheckout.exception.AffLinkNotSupportedException;
import com.autocheckout.exception.BotException;
import com.autocheckout.exception.BussinessException;
import com.autocheckout.http.HttpConnector;
import com.autocheckout.util.StringUtils;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.cookie.ClientCookie;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.jsoup.Jsoup;

public class AffiliateService {

    private final static String DEFAULT_COOKIE_DOMAIN = "lazada.vn";

    private final static String DEFAULT_COOKIE_PATH = "/";

    private final HttpConnector httpConnector;

    public AffiliateService(final HttpConnector httpConnector) {
        this.httpConnector = httpConnector;
    }

    public AffiliateService() {
        this.httpConnector = new HttpConnector(null);
    }

    public List<BasicClientCookie> getAffiliateCookies(final String productUrl) throws BussinessException {
        final Map<String, String> queryParams = new LinkedHashMap<>();
        try {
            final URL url = new URL(productUrl);
            final String query = url.getQuery();
            final String[] params = query.split("&");
            for (final String param : params) {
                final int idx = param.indexOf("=");
                if (idx == -1) {
                    continue;
                }
                queryParams.put(decodeValue(param.substring(0, idx)), decodeValue(param.substring(idx + 1)));
            }
        } catch (final MalformedURLException e) {
            throw new BussinessException("Invalid product link");
        }
        final List<BasicClientCookie> cookies = new ArrayList<>();
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 1);
        cookies.add(createCookie("affiliate_id", queryParams.get("affiliate_id"), calendar.getTime()));
        cookies.add(createCookie("offer_id", queryParams.get("offer_id"), calendar.getTime()));
        cookies.add(createCookie("transaction_id", queryParams.get("transaction_id"), calendar.getTime()));
        return cookies;
    }

    public String getProductUrlFromAffUrl(final String affUrl) throws BotException {
        final String encodedAffUrl = affUrl.replaceAll(" ", "+");
        if (encodedAffUrl.contains("fast.accesstrade")) {
            final String redirectUrl = Jsoup.parse(httpConnector.doGet(encodedAffUrl)).select("head meta[property=og:url]").first().attr("content");
            return httpConnector.doFoward(redirectUrl);
        } else if (encodedAffUrl.contains("go.ecotrackings")) {
            return httpConnector.doFoward(encodedAffUrl);
        } else if (encodedAffUrl.contains("go.masoffer")) {
            final String redirectUrl = Jsoup.parse(httpConnector.doGet(encodedAffUrl)).select("body a").first().attr("href");
            return httpConnector.doFoward(redirectUrl);
        } else {
            LOG.debug("Aff link is not supported...");
            throw new AffLinkNotSupportedException("Aff link is not supported...");
        }
    }

    public boolean isAffUrl(final String url) {
        if (StringUtils.isBlank(url)) {
            return false;
        }
        return url.contains("fast.accesstrade")
            || url.contains("go.ecotrackings")
            || url.contains("go.masoffer");
    }

    private BasicClientCookie createCookie(final String cookieName, final String cookieValue, final Date expiryDate) {
        final BasicClientCookie cookie = new BasicClientCookie(cookieName, cookieValue);
        cookie.setDomain(DEFAULT_COOKIE_DOMAIN);
        cookie.setPath(DEFAULT_COOKIE_PATH);
        cookie.setExpiryDate(expiryDate);
        cookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
        return cookie;
    }

    private String decodeValue(final String value) {
        try {
            return URLDecoder.decode(value, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            // Not reachable
            return null;
        }
    }

}
