/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.service.affiliate;

import com.autocheckout.util.CircularList;
import com.autocheckout.util.StringUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author anhdtc-jp
 */
public class GenerateAffLinkService {

    private static CircularList<String> afflink;

//    public static String generateLink(String encodedAffUrl) {
//
//    }
    public static String generateEcoLink(String url, String sub1, String sub2, String sub3,
        String sub4, String networkId, String sku, String keyword) {
        sub1 = sub1 == null ? "" : sub1;
        sub2 = sub2 == null ? "" : sub2;
        sub3 = sub3 == null ? "" : sub3;
        sub4 = sub4 == null ? "" : sub4;
        networkId = networkId == null ? "" : networkId;
        sku = sku == null ? "" : sku;
        keyword = keyword == null ? "" : keyword;

        AffiliateKey ecomobileToken = GetAffTokenIdService.getEcomobileToken();

        if (!StringUtils.isUrlEncoded(url)) {
            try {
                url = URLEncoder.encode(url, StandardCharsets.UTF_8.toString());
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        }
        String economyLink = "http://go.ecotrackings.com/"
            + "?token=%s"
            + "&url=%s"
            + "&sub1=%s"
            + "&sub2=%s"
            + "&sub3=%s"
            + "&sub4=%s"
            + "&network_id=%s"
            + "&sku=%s"
            + "&keyword=%s";

        return String.format(economyLink, ecomobileToken.getTokenKey(), url, sub1,
            sub2, sub3, sub4, networkId, sku, keyword);
    }

    public static String generateMasofferLink(String url, String sub1, String sub2, String sub3,
        String sub4) {

        AffiliateKey massofferKey = GetAffTokenIdService.getMasofferToken();

        if (massofferKey != null) {

            if (!StringUtils.isUrlEncoded(url)) {
                try {
                    url = URLEncoder.encode(url, StandardCharsets.UTF_8.toString());
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }

            sub1 = sub1 == null ? "" : sub1;
            sub2 = sub2 == null ? "" : sub2;
            sub3 = sub3 == null ? "" : sub3;
            sub4 = sub4 == null ? "" : sub4;

            String masofferFormat = "http://go.masoffer.net/v1/"
                + "%s"
                + "?url=%s"
                + "&aff_sub1=%s"
                + "&aff_sub2=%s"
                + "&aff_sub3=%s"
                + "&aff_sub4=%s";
            return String.format(masofferFormat, massofferKey.getTokenKey(), url, sub1, sub2, sub3, sub4);
        } else {
            return null;
        }
    }

    public static String generateAccesstradeLink(String url, String utmSource, String utmMedium, String utmCampaign,
        String utmContent) {

        String atFormat = "https://pub.accesstrade.vn/deep_link/"
            + "%s"
            + "?url=%s"
            + "&utm_source=%s"
            + "&utm_medium=%s"
            + "&utm_campaign=%s"
            + "&utm_content=%s";

        AffiliateKey atKey = GetAffTokenIdService.getATToken();

        if (atKey != null) {

            if (!StringUtils.isUrlEncoded(url)) {
                try {
                    url = URLEncoder.encode(url, StandardCharsets.UTF_8.toString());
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }

            utmSource = utmSource == null ? "" : utmSource;
            utmMedium = utmMedium == null ? "" : utmMedium;
            utmCampaign = utmCampaign == null ? "" : utmCampaign;
            utmContent = utmContent == null ? "" : utmContent;

            return String.format(atFormat, atKey, url, utmSource, utmMedium, utmCampaign, utmContent);
        } else {
            return null;
        }
    }
}
