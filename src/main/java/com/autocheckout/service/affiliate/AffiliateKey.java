/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.service.affiliate;

/**
 *
 * @author anhdtc-jp
 */
public class AffiliateKey {

    public static final int MASOFFER = 1;
    public static final int ECOMOBI = 2;
    public static final int ACCESSTRADE = 3;

    private int type;
    private String tokenKey;

    public AffiliateKey(int type, String tokenKey) {
        this.type = type;
        this.tokenKey = tokenKey;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

}
