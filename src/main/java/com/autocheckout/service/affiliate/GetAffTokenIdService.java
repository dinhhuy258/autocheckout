/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.service.affiliate;

import com.autocheckout.LOG;
import java.util.ArrayList;

/**
 *
 * @author anhdtc-jp
 */
public class GetAffTokenIdService {

    private static final ArrayList<AffiliateKey> DATA_SOURCE;
    private static ArrayList<AffiliateKey> dataSource;

    private static final int ACCESSTRADE_FIRST = 3;
    private static final int ECOMOBILE_FIRST = 2;
    private static final int MASOFFER_FIRST = 1;

    static {

        // Initial data for AT
        DATA_SOURCE = new ArrayList<>();
        //Add token to source
        DATA_SOURCE.add(new AffiliateKey(AffiliateKey.ACCESSTRADE, "4788671657591529770"));
        //
        //TODO Add more...
        //
        // Initial data for Masoffer
        //Add token to source
        DATA_SOURCE.add(new AffiliateKey(AffiliateKey.MASOFFER, "KSWejj1jrPqPwl7VPVvz9dzoSCrRulJMCXHzeym0sXw"));
        //
        //TODO Add more...
        //
        // Initial data for Ecomobi
        //Add token to source
        DATA_SOURCE.add(new AffiliateKey(AffiliateKey.ECOMOBI, "ETsmipysJcWoVpBUXOwkL"));
        //
        //TODO Add more...
        //
        //Copy to source
        dataSource = new ArrayList<>(DATA_SOURCE);
    }

    public static AffiliateKey getToken() {

        AffiliateKey rs = null;

        if (dataSource.size() < 1) {
            //Copy to source
            dataSource = new ArrayList<>(DATA_SOURCE);
        }

        if (dataSource.size() >= 1) {
            rs = dataSource.get(0);
            dataSource.remove(0);
        }

        LOG.debug("GetTokenByType : " + (rs != null ? rs.getTokenKey() : " Khong tim thay key nao."));

        return rs;
    }

    private static AffiliateKey getTokenByType(int tokenType) {

        AffiliateKey rs = null;

        int size = dataSource.size();

        for (int i = 0; i < size; i++) {
            AffiliateKey e = dataSource.get(i);
            if (e.getType() == tokenType) {
                rs = e;
                dataSource.remove(e);
                break;
            }
        }

        if (rs == null) {

            size = DATA_SOURCE.size();
            for (int i = 0; i < size; i++) {
                AffiliateKey e = DATA_SOURCE.get(i);
                if (e.getType() == tokenType) {

                    if (rs == null) {
                        rs = new AffiliateKey(e.getType(), e.getTokenKey());
                        continue;
                    }

                    dataSource.add(new AffiliateKey(e.getType(), e.getTokenKey()));
                }
            }
        }

        LOG.debug("GetTokenByType :" + tokenType + " result: "
            + (rs != null ? rs.getTokenKey() : " Khong tim thay key nao."));

        return rs;
    }

    public static AffiliateKey getATToken() {

        return getTokenByType(AffiliateKey.ACCESSTRADE);
    }

    public static AffiliateKey getEcomobileToken() {
        return getTokenByType(AffiliateKey.ECOMOBI);
    }

    public static AffiliateKey getMasofferToken() {
        return getTokenByType(AffiliateKey.MASOFFER);
    }
}
