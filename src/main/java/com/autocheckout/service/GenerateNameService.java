/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.service;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.account.FullName;
import com.autocheckout.util.FileUtils;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author anhdtc-jp
 */
public class GenerateNameService {
// Store First Name

    private static final ArrayList<String> hoList;

    // Store Sub Name
    private static final ArrayList<String> subNameList;

    // Store static  Name
    private static final ArrayList<String> nameList;

    static {
        hoList = FileUtils.importFile(LazadaConstant.FILE_PATH.PARENT_NAME_FILE_PATH);
        subNameList = FileUtils.importFile(LazadaConstant.FILE_PATH.SUB_NAME_FILE_PATH);
        nameList = FileUtils.importFile(LazadaConstant.FILE_PATH.NAME_FILE_PATH);
        initData();
    }

    private static void initData() {
        hoList.add(LazadaConstant.DEFAULT_VALUE.FIRSTNAME);
        subNameList.add(LazadaConstant.DEFAULT_VALUE.SUB_NAME);
        nameList.add(LazadaConstant.DEFAULT_VALUE.NAME);
    }

    public static ArrayList<FullName> generateName(final int numberOfName) {
        LOG.debug("com.autocheckout.generateservice.AutoGenerateNameService.generateName()");

        final int nameSize = nameList.size();
        final int firstNameSize = hoList.size();
        final int subNameSize = subNameList.size();
        final ArrayList<FullName> rs = new ArrayList<>();
        int randomFirstName;
        int randomSubName;
        int randomName;
        String fullName;

        final Random rn = new Random();

        for (int i = 0; i < numberOfName; i++) {
            try {
                randomFirstName = rn.nextInt(firstNameSize);
                randomSubName = rn.nextInt(subNameSize);
                randomName = rn.nextInt(nameSize);

            } catch (final Exception e) {
                LOG.debug("generateName Fail on Random :" + e.getMessage());

                // Default Name
                randomFirstName = 0;
                randomSubName = 0;
                randomName = 0;
            }

            fullName = hoList.get(randomFirstName) + " " + subNameList.get(randomSubName)
                + " " + nameList.get(randomName);

            rs.add(new FullName(hoList.get(randomFirstName), subNameList.get(randomSubName),
                nameList.get(randomName), fullName));
        }
        return rs;
    }
}
