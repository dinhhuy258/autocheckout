/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.service;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.account.FullName;
import com.autocheckout.data.lazada.account.RegistrationAccountInfo;
import com.autocheckout.util.FileUtils;
import com.autocheckout.util.StringUtils;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author anhdtc-jp
 */
public class GenerateAccountService {

    // Store Email Domain
    private final ArrayList<String> emailDomain;

    public GenerateAccountService() {

        emailDomain = FileUtils.importFile(LazadaConstant.FILE_PATH.EMAIL_DOMAIN_PATH);
        emailDomain.add(LazadaConstant.DEFAULT_VALUE.EMAIL_DOMAIN);
    }

    public ArrayList<RegistrationAccountInfo> generateRegistrationAccountInfo(final int soLuong) {

        LOG.debug("com.autocheckout.generateservice.AutoGenerateAccount.generateName()");
        final ArrayList<RegistrationAccountInfo> result = new ArrayList<>();
        final GenerateNameService autoGenerateNameService = new GenerateNameService();

        final ArrayList<FullName> newNameList = autoGenerateNameService.generateName(soLuong);

        final int emailDomainSize = emailDomain.size();
        int emailDomainIndex;
        int randomMonth;
        int randomDay;
        int randomYear;
        String birthday;
        String day;
        String month;
        String year;
        String fullName;
        String email;

        final Random rn = new Random();

        for (int i = 0; i < soLuong; i++) {

            final FullName newName = newNameList.get(i);

            try {
                randomDay = 1 + rn.nextInt(27);
                randomMonth = 1 + rn.nextInt(11);
                randomYear = 1950 + rn.nextInt(60);
                emailDomainIndex = rn.nextInt(emailDomainSize);

            } catch (final Exception e) {
                LOG.debug("generateName Fail on Random :" + e.getMessage());

                // Default information
                randomDay = 1;
                randomMonth = 1;
                randomYear = 1950;
                emailDomainIndex = 0;
            }

            int randomEmailNumber;

            try {
                randomEmailNumber = rn.nextInt(10000);
            } catch (final Exception e) {
                LOG.debug("generate email Fail on Random :" + e.getMessage());

                // Default Name
                randomEmailNumber = 0;
            }
            email = (StringUtils.convertToLatin(newName.getFullName()) + randomEmailNumber
                + emailDomain.get(emailDomainIndex)).replaceAll(" ", "");

            birthday = randomYear + "-" + randomMonth + "-" + randomDay;
            day = randomDay + "";
            month = randomMonth + "";
            year = randomYear + "";

            final RegistrationAccountInfo newUser = new RegistrationAccountInfo(email,
                LazadaConstant.DEFAULT_VALUE.PASSWORD, LazadaConstant.DEFAULT_VALUE.PASSWORD,
                newName.getFullName(), month, day, year, birthday, LazadaConstant.DEFAULT_VALUE.GENDER);

            result.add(newUser);
        }

        LOG.debug("com.autocheckout.generateservice.AutoGenerateAccount.generateName() END");
        return result;
    }
}
