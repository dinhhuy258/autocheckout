/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autocheckout.service;

import com.autocheckout.GUI.LOGGER;
import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.account.Address;
import com.autocheckout.util.FileUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javax.swing.JFrame;

/**
 *
 * @author anhdtc-jp
 */
public final class GenerateAddressService {

    private static final Address VIETNAM;
    private static final ArrayList<Address> CITY_LIST;
    private static final ArrayList<Address> PREFECTURE_LIST;
    private static final ArrayList<Address> TOWN_LIST;
    private static ArrayList<String> DETAIL_LIST;
    private static final Random RANDOM;

    private static final JFrame logFr;

    private GenerateAddressService() {
        // Avoid create new instance
    }

    static {
        CITY_LIST = new ArrayList<>();
        PREFECTURE_LIST = new ArrayList<>();
        TOWN_LIST = new ArrayList<>();
        DETAIL_LIST = FileUtils.importFile(LazadaConstant.FILE_PATH.DETAILS_ADDRESS_DATA_PATH);
        RANDOM = new Random();

        // Initial Root Address
        VIETNAM = new Address(-1, null, "R49915", "Việt Nam", "Việt Nam", "Việt Nam");
//        initDataQuan6();
        LOG.debug("com.autocheckout.generateservice.AutoGenerateAddress. START INIT");
        initData();
        logFr = new LOGGER();
        logFr.setVisible(true);

    }

    public static void setDetailAddress(String fileName) {
        DETAIL_LIST = FileUtils.importFileFullPath(fileName);
    }

    private static void initData() {

        LOG.debug("START init data for Address");
        final ArrayList<String> inputStr = FileUtils.importFile(LazadaConstant.FILE_PATH.ADDRESS_DATA_PATH);

        // Init City List
        initAddressData(inputStr, CITY_LIST, 0);
        initAddressData(inputStr, PREFECTURE_LIST, 1);
        initAddressData(inputStr, TOWN_LIST, 2);

    }

    private static void initDataQuan6() {
        final Address tpHCM = new Address(1, VIETNAM, "R1973756", "Hồ Chí Minh", "Hồ Chí Minh", "");
        final Address quan6 = new Address(2, tpHCM, "R6228792", "Quận 6", "Quận 6", "");
        Address phuong = new Address(3, quan6, "R6850613", "Phường 1", "Phường 1", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6850613", "Phường 1", "Phường 1", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6850614", "Phường 2", "Phường 2", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856465", "Phường 3", "Phường 3", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6850979", "Phường 4", "Phường 4", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856464", "Phường 5", "Phường 5", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6850976", "Phường 6", "Phường 6", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856467", "Phường 7", "Phường 7", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856466", "Phường 8", "Phường 8", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6846117", "Phường 9", "Phường 9", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856468", "Phường 10", "Phường 10", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856469", "Phường 11", "Phường 11", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856470", "Phường 12", "Phường 12", "");
        TOWN_LIST.add(phuong);
        phuong = new Address(3, quan6, "R6856471", "Phường 13", "Phường 13", "");
        TOWN_LIST.add(phuong);
    }

    public static ArrayList<Address> getCityList(final String countryId) {

        if (countryId == null) {
            return CITY_LIST;
        }

        if (!VIETNAM.getAddressID().equals(countryId)) {
            LOG.debug("Only suport for Vietnam, please try again late.");
            return new ArrayList<>();
        }

        if (CITY_LIST.size() < 1) {
            LOG.debug("Database is Emtpy, please run importData method first.");
            return new ArrayList<>();
        }

        return CITY_LIST;
    }

    public static ArrayList<Address> getCityList() {
        return getCityList(null);
    }

    public static ArrayList<Address> getListPrefectureOfCity(final Address city) {

        if (city != null) {

            return getListPrefectureOfCity(city.getAddressID());
        }

        LOG.debug("Can't generate address by " + city + " => return empty array.");
        return new ArrayList<>();
    }

    public static ArrayList<Address> getListPrefectureOfCity(final String cityId) {

        final ArrayList<Address> resultList = new ArrayList<>();

        if (PREFECTURE_LIST.size() < 1) {

            LOG.debug("Database is Emtpy, please run importData method first.");
            return resultList;
        }

        final Iterator<Address> iter = PREFECTURE_LIST.iterator();

        while (iter.hasNext()) {

            final Address prefec = (Address) iter.next();

            if ((prefec.getParentAddress().getAddressID() != null)
                && prefec.getParentAddress().getAddressID().equals(cityId)) {

                resultList.add(prefec);
            }
        }

        LOG.debug("GetPrefectureOfCity :" + cityId + " Found " + resultList.size() + " item.");
        return resultList;
    }

    public static ArrayList<Address> getListTownOfPrefecture(final Address prefectureId) {

        if (prefectureId != null) {

            return GenerateAddressService.getListTownOfPrefecture(prefectureId.getAddressID());
        }

        LOG.debug("Can't generate address by " + prefectureId + " => return empty array.");
        return new ArrayList<>();

    }

    public static ArrayList<Address> getListTownOfPrefecture(final String prefectureId) {

        final ArrayList<Address> resultList = new ArrayList<>();

        if (TOWN_LIST.size() < 1) {
            LOG.debug("Database is Emtpy, please run importData method first.");
            return resultList;
        }

        final Iterator<Address> iter = TOWN_LIST.iterator();

        while (iter.hasNext()) {

            final Address town = (Address) iter.next();

            if ((town.getParentAddress().getAddressID() != null)
                && town.getParentAddress().getAddressID().equals(prefectureId)) {
                resultList.add(town);
            }
        }

        LOG.debug("ListTownOfPrefecture :" + prefectureId + " Found " + resultList.size() + " item.");
        return resultList;
    }

    public static String generateDetailsAddress() {

        LOG.debug("com.autocheckout.service.GenerateAddressService.generateDetailsAddress()");
        LOG.debug("Size : " + DETAIL_LIST.size());
        final int detailIndex = RANDOM.nextInt(DETAIL_LIST.size());

        return DETAIL_LIST.get(detailIndex);
    }

    public static ArrayList<Address> generateByTown(final String townId, final int numberOfAddress) {

        if (townId != null) {

            for (final Address item : TOWN_LIST) {

                if (townId.equals(item.getAddressID())) {

                    return generateByTown(item, numberOfAddress);
                }
            }
        }

        LOG.debug("Can't generate address by " + townId + " => return empty array.");
        return new ArrayList<>();
    }

    public static ArrayList<Address> generateByTown(final Address town, final int numberOfAddress) {

        LOG.debug("com.autocheckout.service.GenerateAddressService.generateByTown()");
        final ArrayList<Address> rs = new ArrayList<>();

        for (int i = 0; i < numberOfAddress; i++) {

            // Random a town
            final Address newItem = new Address(town);

            // Set address type to 3: had details address
            newItem.setType(3);

            // Random a address detail
            newItem.setAddressDetail(generateDetailsAddress());

            rs.add(newItem);
        }
        return rs;
    }

    public static ArrayList<Address> generateByPrefecture(final String prefecId, final int numberOfAddress) {

        if (prefecId != null) {

            for (final Address item : PREFECTURE_LIST) {

                if (prefecId.equals(item.getAddressID())) {

                    return generateByPrefecture(item, numberOfAddress);
                }
            }
        }

        LOG.debug("Can't generate address by " + prefecId + " => return empty array.");
        return new ArrayList<>();
    }

    public static ArrayList<Address> generateByPrefecture(final Address prefec, final int numberOfAddress) {

        final ArrayList<Address> rs = new ArrayList<>();

        // Get town list of prefec.
        final ArrayList<Address> townOfPrefec = getListTownOfPrefecture(prefec);

        final int listSize = townOfPrefec.size();
        for (int i = 0; i < numberOfAddress; i++) {

            // Random a town
            rs.addAll(generateByTown(townOfPrefec.get(RANDOM.nextInt(listSize)), 1));
        }
        return rs;
    }

    public static ArrayList<Address> generateByCity(final String cityId, final int numberOfAddress) {

        if (cityId != null) {

            for (final Address item : CITY_LIST) {

                if (cityId.equals(item.getAddressID())) {

                    return generateByCity(item, numberOfAddress);
                }
            }
        }

        LOG.debug("Cant find address * " + cityId);
        return new ArrayList<>();
    }

    public static ArrayList<Address> generateByCity(final Address city, final int numberOfAddress) {
        System.out.print("com.autocheckout.service.GenerateAddressService.generateByCity()  " + city.getAddressName() + "  ");
        final ArrayList<Address> result = new ArrayList<>();

        // Get town list of prefec.
        final ArrayList<Address> prefecOfCity = GenerateAddressService.getListPrefectureOfCity(city);
        LOG.debug(prefecOfCity.size());
        final int listSize = prefecOfCity.size();
        for (int i = 0; i < numberOfAddress; i++) {

            // Random a prefecture
            final Address randomPrefec = prefecOfCity.get(RANDOM.nextInt(listSize));

            result.addAll(generateByPrefecture(randomPrefec, 1));
        }

        return result;
    }

    public static ArrayList<Address> generateByNational(final int numberOfAddress) {

        return generateByNational(null, numberOfAddress);
    }

    public static ArrayList<Address> generateByNational(final String nationalId, final int numberOfAddress) {

        final ArrayList<Address> result = new ArrayList<>();

        final int listSize = CITY_LIST.size();

        for (int i = 0; i < numberOfAddress; i++) {

            // Random a city
            result.addAll(generateByCity(CITY_LIST.get(RANDOM.nextInt(listSize)), 1));
        }

        // Return
        return result;
    }

    private static void initAddressData(final ArrayList<String> inputList, final ArrayList<Address> tartgetList, final int tartgetType) {
        LOG.debug("com.autocheckout.generateservice.AutoGenerateAddress.initAddressData() START for " + tartgetType);
        LOG.debug("Input : " + inputList.size() + " " + inputList.size());
        final Iterator<String> iter = inputList.iterator();
        while (iter.hasNext()) {
            final String addressStr = (String) iter.next();
            LOG.debug(addressStr);
            final String[] addressPlit = addressStr.split("/");

            final int type = Integer.parseInt(addressPlit[0]);
            if (type != tartgetType) {
                continue;
            }
            final String addressId = addressPlit[1];
            final String parrentId = addressPlit[2];
            final String addressName = addressPlit[3];
            final String addressDisplayName = addressPlit[4];
            final String addressLocalName = "";

            // Address(int type, Address parentAddress, String addressID, String addressName, String displayName, String nameLocal)
            final Address item = new Address(type, searchParentID(type - 1, parrentId), addressId, addressName, addressDisplayName, parrentId);
            tartgetList.add(item);
//            inputList.remove(addressStr);
            iter.remove();
        }

        LOG.debug("com.autocheckout.generateservice.AutoGenerateAddress.initAddressData() END");
    }

    private static Address searchParentID(final int parentType, final String parentId) {
        LOG.debug("com.autocheckout.generateservice.AutoGenerateAddress.searchParentID() START");
        LOG.debug("Type: " + parentType + " ID : " + parentId);
        final ArrayList<Address> tartgetSearch;
        switch (parentType) {
            case 0:
                tartgetSearch = CITY_LIST;
                break;
            case 1:
                tartgetSearch = PREFECTURE_LIST;
                break;
            default:
                LOG.debug("com.autocheckout.generateservice.AutoGenerateAddress.searchParentID()VietNam END");
                return VIETNAM;
        }

        for (final Address item : tartgetSearch) {

            if (item.getAddressID().equals(parentId)) {
                LOG.debug(
                    "com.autocheckout.generateservice.AutoGenerateAddress.searchParentID() END" + item.getAddressName());
                return item;
            }
        }
        LOG.debug("com.autocheckout.generateservice.AutoGenerateAddress.searchParentID()END NULL");
        return null;
    }

    public static ArrayList<Address> generateNewAddress(final int numberOfAddress) {

        final ArrayList<Address> rs = new ArrayList<>();

        for (int i = 0; i < numberOfAddress; i++) {

            final int index = RANDOM.nextInt(TOWN_LIST.size());
            final Address address = new Address(TOWN_LIST.get(index));

            final String detaiAddress = DETAIL_LIST.get(RANDOM.nextInt(DETAIL_LIST.size()));

            address.setAddressDetail(detaiAddress);
            address.setType(3);
            rs.add(address);
        }

        return rs;
    }
}
