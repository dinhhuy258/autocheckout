package com.autocheckout.service.lazada;

import com.autocheckout.LOG;
import com.autocheckout.exception.BotException;
import com.autocheckout.http.HttpConnector;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class LazadaProductService {

    private final HttpConnector httpConnector;

    public LazadaProductService(final HttpConnector httpConnector) {
        this.httpConnector = httpConnector;
    }

    public boolean isProductOutOfStock(final String productUrl) {
        try {
            final Document document = Jsoup.parse(httpConnector.doGet(productUrl));
            Elements elements = document.select(".quantity-content-warning");
            if (elements.size() != 0) {
                final String content = elements.first().text();
                if ("Hết hàng".equalsIgnoreCase(content) || "Out of stock".equalsIgnoreCase(content)) {
                    return true;
                }
            }
            elements = document.select("button.pdp-button");
            if (elements.size() != 0) {
                final String content = elements.first().text();
                if ("HÀNG SẮP VỀ".equalsIgnoreCase(content) || "COMING SOON".equalsIgnoreCase(content)) {
                    return true;
                }
            }
            elements = document.select(".quantity-content-default");
            if (elements.size() != 0) {
                final String content = elements.first().text();
                if (content == null || "".equals(content)) {
                    return false;
                }
                return true;
            }
            return false;
        } catch (final BotException e) {
            LOG.debug("Check product error: " + e.getMessage());
            return true;
        }
    }
}
