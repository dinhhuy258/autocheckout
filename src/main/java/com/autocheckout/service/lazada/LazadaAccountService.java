package com.autocheckout.service.lazada;

import com.autocheckout.LOG;
import com.autocheckout.constant.LazadaConstant;
import com.autocheckout.data.lazada.account.CheckMailApiResponse;
import com.autocheckout.http.HttpConnector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class LazadaAccountService {

    private final HttpConnector httpConnector;

    private final Gson gson;

    public LazadaAccountService(final HttpConnector httpConnector) {
        this.httpConnector = httpConnector;
        gson = new GsonBuilder().create();
    }

    public boolean isAccountExist(final String email) {
        try {
            final String checkEmailResponse = httpConnector.doGet(LazadaConstant.CHECK_EMAIL + email);
            final CheckMailApiResponse apiResponse = gson.fromJson(checkEmailResponse, CheckMailApiResponse.class);
            return apiResponse != null && apiResponse.getSuccess() && LazadaConstant.DEFAULT_VALUE.CHECK_MAIL_OK.equalsIgnoreCase(apiResponse.getModule().getResult());
        } catch (final Exception e) {
            LOG.debug("Some error happen while checking email :" + e.getMessage());
            return false;
        }
    }
}
