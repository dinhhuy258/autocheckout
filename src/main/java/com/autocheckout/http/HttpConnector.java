package com.autocheckout.http;

import com.autocheckout.exception.BotException;
import com.autocheckout.exception.NetworkException;
import com.autocheckout.exception.ProxyException;
import com.autocheckout.exception.TechniqueException;
import com.autocheckout.parser.ProxyParser;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.cookie.ClientCookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;

public class HttpConnector {

    private static final int DEFAULT_CONNECT_TIMEOUT = 10000;

    private static final int DEFAULT_SOCKET_TIMEOUT = 10000;

    private static final int DEFAULT_CONNECTION_REQUEST_TIMEOUT = 10000;

    private final HttpGet httpGet;

    private final HttpPost httpPost;

    private final RequestConfig requestConfig;

    private final CookieStore cookieStore;

    private final LaxRedirectStrategy laxRedirectStrategy;

    private CloseableHttpClient httpClient;

    private DefaultProxyRoutePlanner proxyRoutePlanner;

    private CredentialsProvider credentialsProvider;

    private boolean hasProxy;

    public HttpConnector() {
        this(null);
    }

    public HttpConnector(final String proxy) {
        proxyRoutePlanner = null;
        credentialsProvider = null;
        if (proxy != null) {
            hasProxy = true;
            final ProxyParser proxyParser = new ProxyParser(proxy);
            final HttpHost proxyHost = new HttpHost(proxyParser.getProxyHost(), proxyParser.getProxyPort(), "http");
            proxyRoutePlanner = new DefaultProxyRoutePlanner(proxyHost);
            if (proxyParser.hasAuthentication()) {
                credentialsProvider = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(new AuthScope(proxyParser.getProxyHost(), proxyParser.getProxyPort()),
                    new UsernamePasswordCredentials(proxyParser.getProxyUser(), proxyParser.getProxyPass()));
            }
        }
        laxRedirectStrategy = new LaxRedirectStrategy();
        cookieStore = new BasicCookieStore();
        requestConfig = RequestConfig.custom()
            .setConnectTimeout(DEFAULT_CONNECT_TIMEOUT)
            .setSocketTimeout(DEFAULT_SOCKET_TIMEOUT)
            .setConnectionRequestTimeout(DEFAULT_CONNECTION_REQUEST_TIMEOUT)
            .setCookieSpec(CookieSpecs.STANDARD)
            .build();
        httpClient = HttpClientBuilder.create()
            .setDefaultRequestConfig(requestConfig)
            .setDefaultCookieStore(cookieStore)
            .setRedirectStrategy(laxRedirectStrategy)
            .setDefaultCredentialsProvider(credentialsProvider)
            .setRoutePlanner(proxyRoutePlanner)
            .build();
        final String userAgent = RandomUserAgent.getRandomUserAgent();
        httpGet = new HttpGet();
        httpGet.setHeader(HttpHeaders.USER_AGENT, userAgent);
        httpPost = new HttpPost();
        httpPost.setHeader(HttpHeaders.USER_AGENT, userAgent);
        httpPost.setHeader(HttpHeaders.CONNECTION, "keep-alive");
        httpPost.setHeader(HttpHeaders.CACHE_CONTROL, "max-age=0");
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
    }

    public String doFoward(final String url) throws BotException {
        httpGet.setURI(URI.create(url));
        final HttpClientContext context = HttpClientContext.create();
        try {
            httpClient.execute(httpGet, context);
            final HttpHost target = context.getTargetHost();
            final List<URI> redirectLocations = context.getRedirectLocations();
            final URI location = URIUtils.resolve(httpGet.getURI(), target, redirectLocations);
            return location.toString();
        } catch (final IOException e) {
            throw new NetworkException("There is problem while connecting to server", e);
        } catch (final Exception e) {
            throw new TechniqueException("There is problem while forwarding url: " + url);
        }
    }

    public String doGet(final String url, final String referer) throws BotException {
        httpGet.setURI(URI.create(url));
        httpGet.setHeader(HttpHeaders.REFERER, referer);
        final String result = execute(httpGet);
        httpGet.removeHeaders(HttpHeaders.REFERER);
        return result;
    }

    public String doGet(final String url) throws BotException {
        httpGet.setURI(URI.create(url));
        return execute(httpGet);
    }

    public String doPost(final String url, final String body, final String csrfToken, final String referer) throws BotException {
        httpPost.setURI(URI.create(url));
        httpPost.setHeader("X-CSRF-TOKEN", csrfToken);
        httpGet.setHeader(HttpHeaders.REFERER, referer);
        httpPost.setEntity(new StringEntity(body, "UTF-8"));
        final String result = execute(httpPost);
        httpGet.removeHeaders(HttpHeaders.REFERER);
        return result;
    }

    public String doPost(final String url, final String body, final String csrfToken) throws BotException {
        httpPost.setURI(URI.create(url));
        httpPost.setHeader("X-CSRF-TOKEN", csrfToken);
        httpPost.setEntity(new StringEntity(body, "UTF-8"));
        return execute(httpPost);
    }

    private String execute(final HttpUriRequest request) throws BotException {
        CloseableHttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(request);
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN) {
                    throw new ProxyException("Proxy is invalid");
                }
                throw new TechniqueException("Invalid status code: " + httpResponse.getStatusLine().getStatusCode());
            }
            return EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
        } catch (final IOException e) {
            throw new NetworkException("There is problem while connecting to server", e);
        } finally {
            try {
                if (httpResponse != null) {
                    httpResponse.close();
                }
            } catch (final IOException e) {
                throw new NetworkException("There is problem while closing HttpResponse", e);
            }
        }
    }

    public void clearCookies() {
        cookieStore.clear();
    }

    public void addCookies(final List<BasicClientCookie> cookies) {
        for (final BasicClientCookie cookie : cookies) {
            cookieStore.addCookie(cookie);
        }
    }

    public void addCookie(final String cookieName, final String cookieValue, final String domain, final String path, final Date expiryDate) {
        final BasicClientCookie cookie = new BasicClientCookie(cookieName, cookieValue);
        cookie.setDomain(domain);
        cookie.setPath(path);
        cookie.setExpiryDate(expiryDate);
        cookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
    }

    public boolean hasProxy() {
        return hasProxy;
    }

    public void removeProxy() {
        if (hasProxy) {
            hasProxy = false;
            httpClient = HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(cookieStore)
                .setRedirectStrategy(laxRedirectStrategy)
                .setDefaultCookieStore(cookieStore)
                .build();
        }
    }
}
