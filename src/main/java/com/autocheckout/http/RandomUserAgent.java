package com.autocheckout.http;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class RandomUserAgent {

    private static final Map<String, String[]> uaMap = new HashMap<String, String[]>();

    private static final Map<String, Double> freqMap = new HashMap<String, Double>();

    static {
        freqMap.put("Internet Explorer", 15.0);
        freqMap.put("Firefox", 20.0);
        freqMap.put("Chrome", 50.0);
        freqMap.put("Safari", 15.0);

        uaMap.put("Internet Explorer", new String[] {
            "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/13.10586"
        });
        uaMap.put("Firefox", new String[] {
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:59.0) Gecko/20100101 Firefox/59.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/59.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/59.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/59.0",
            "Mozilla/5.0 (X11; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/59.0",
            "Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:27.0) Gecko/20121011 Firefox/59.0"
        });
        uaMap.put("Chrome", new String[] {
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"
        });
        uaMap.put("Safari", new String[] {
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.5.6 (KHTML, like Gecko) Version/11.0.3 Safari/604.5.6",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/11.0.3 Safari/604.5.6"
        });
    }

    public static String getRandomUserAgent() {
        final double rand = Math.random() * 100;
        String browser = null;
        double count = 0.0;
        for(final Entry<String, Double> freq : freqMap.entrySet()) {
            count += freq.getValue();
            if(rand <= count) {
                browser = freq.getKey();
                break;
            }
        }
        if(browser == null) {
            browser = "Chrome";
        }
        final String[] userAgents = uaMap.get(browser);
        return userAgents[(int) Math.floor(Math.random() * userAgents.length)];
    }
}

