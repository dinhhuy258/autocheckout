package com.autocheckout.exception;

public class NetworkException extends BotException {

    public NetworkException(final String msg) {
        super(msg);
    }

    public NetworkException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
