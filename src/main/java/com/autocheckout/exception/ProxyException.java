package com.autocheckout.exception;

public class ProxyException extends NetworkException {

    public ProxyException(final String msg) {
        super(msg);
    }

    public ProxyException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
