package com.autocheckout.exception;

public class AffLinkNotSupportedException extends BussinessException {

    public AffLinkNotSupportedException(final String msg) {
        super(msg);
    }

    public AffLinkNotSupportedException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
