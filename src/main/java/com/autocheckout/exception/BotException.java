package com.autocheckout.exception;

public class BotException extends Exception {

    public BotException(final String msg) {
        super(msg);
    }

    public BotException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
