package com.autocheckout.exception;

public class BussinessException extends BotException {

    public BussinessException(final String msg) {
        super(msg);
    }

    public BussinessException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
