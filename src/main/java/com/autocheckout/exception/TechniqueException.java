package com.autocheckout.exception;

public class TechniqueException extends BotException {

    public TechniqueException(final String msg) {
        super(msg);
    }

    public TechniqueException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
